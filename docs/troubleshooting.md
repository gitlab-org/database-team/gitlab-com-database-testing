# Connecting to database testing runners

The `migration-testing-worker` and `migration-testing-builder` runners work on isolated vms as described in [the readme](../README.md).

These VMs run in google cloud, in the `group-database` project [here](https://console.cloud.google.com/compute/instances?hl=en&project=group-database-239135).

You can connect to these vms for troubleshooting by following the `gcloud` docs - https://cloud.google.com/compute/docs/connect/standard-ssh#gcloud

### Diagnosing out-of-disk-space errors

Either the `migration-testing-worker` or `migration-testing-builder` can fail to run jobs if the entire disk fills with stale docker images.

Cron jobs on both hosts should take care of this automatically.

If you see errors related to disk space, SSH in and check how much space docker is using with `sudo docker system df -v`

To prune outdated images, consider using the `/usr/local/bin/docker-cleanup` script.
If the registry container on the migration worker has accumulated a large amount of data in its volume, consider the `/root/clean_docker_registry.sh` script.

