# frozen_string_literal: true

require 'gitlab'

class GitlabApiClient
  UPSTREAM_SOURCE_PROJECT_REGEX = /\W/.freeze
  API_TOKEN_SUFFIX = '_API_TOKEN'

  def initialize(endpoint)
    @endpoint = endpoint
  end

  def call
    Gitlab.client(endpoint: endpoint, private_token: private_token)
  end

  private

  attr_reader :endpoint

  def private_token
    ENV.fetch(pat_name, ENV['GITLAB_API_TOKEN'])
  end

  def pat_name
    return '' unless ENV['TOP_UPSTREAM_SOURCE_PROJECT']

    ENV['TOP_UPSTREAM_SOURCE_PROJECT'].gsub(UPSTREAM_SOURCE_PROJECT_REGEX, '_').upcase + API_TOKEN_SUFFIX
  end
end
