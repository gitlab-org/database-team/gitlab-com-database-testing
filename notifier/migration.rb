# frozen_string_literal: true

class Migration
  REGULAR_MIGRATION_GUIDANCE_SECONDS = 3.minutes.freeze
  POST_DEPLOY_MIGRATION_GUIDANCE_SECONDS = 10.minutes.freeze
  POST_DEPLOY_CONCURRENT_MIGRATION_GUIDANCE_SECONDS = 20.minutes.freeze
  SRE_NOTIFICATION_GUIDANCE = 20.minutes.freeze
  TYPE_REGULAR = 'regular'
  TYPE_POST_DEPLOY = 'post_deploy'
  TYPE_BACKGROUND_BATCH = 'background_batch'
  TIMING_GUIDELINES = 'https://docs.gitlab.com/ee/development/migration_style_guide.html#how-long-a-migration-should-take'
  POST_DEPLOY_MIGRATION_GUIDE = 'https://docs.gitlab.com/ee/development/post_deployment_migrations.html'
  BATCHED_BACKGROUND_MIGRATION_GUIDE = 'https://docs.gitlab.com/ee/development/database/batched_background_migrations.html'

  def self.background_migration_batch_from_directory(directory)
    query_details_path = directory.join('query-details.json')
    query_details = if File.exist?(query_details_path)
                      JSON.parse(File.read(query_details_path))
                    else
                      []
                    end

    stats = JSON.parse(File.read(directory.join('migration-stats.json')))

    migration_data = {
      version: nil,
      name: directory.basename.to_s,
      type: TYPE_BACKGROUND_BATCH,
      path: nil,
      intro_on_current_branch: true
    }

    new(migration_data, stats, query_details)
  end

  def self.from_directory(directory, global_migration_data:)
    query_details_path = directory.join('query-details.json')
    query_details = if File.exist?(query_details_path)
                      JSON.parse(File.read(query_details_path))
                    else
                      []
                    end

    stats = JSON.parse(File.read(directory.join('migration-stats.json')))
    version = migration_timestamp(stats)

    type, path, intro_on_current_branch = global_migration_data[version.to_s].values_at(
      'type', 'path', 'intro_on_current_branch'
    )
    migration_data = {
      version: version,
      name: directory.basename.to_s, # Otherwise name is a Pathname
      type: type,
      path: path,
      intro_on_current_branch: intro_on_current_branch
    }.stringify_keys

    new(migration_data, stats, query_details)
  end

  # Keeps compatibility with the old +migration-stats.json+ format
  #
  # @example
  #         old_format = { version: 20240612045635, name: 'MyMigration', walltime: 7.124546, ...}
  #
  # @example
  #         new_format = { version: { timestamp: 20240612045635, milestone: {...}, ... } }
  def self.migration_timestamp(stats)
    case stats['version']
    when Integer, String
      stats['version']
    when Hash
      stats['version']['timestamp']
    end
  end

  attr_accessor :version, :name, :statistics, :total_database_size_change,
                :queries, :type, :walltime, :intro_on_current_branch, :success,
                :query_executions, :error_message

  # So that migration durations can be plotted directly in execution histogram charts
  alias_method :duration, :walltime

  def initialize(migration, stats, query_details)
    @version = migration['version']
    @path = migration['path']
    @name = migration['name']
    @type = migration['type']
    @intro_on_current_branch = migration['intro_on_current_branch']
    @query_executions = query_details.map { |qd| QueryExecution.new(qd) }
    init_stats(stats)
  end

  def intro_on_current_branch?
    intro_on_current_branch
  end

  def success?
    @success
  end

  def was_run?
    @was_run
  end

  def regular?
    type == TYPE_REGULAR
  end

  def post_deploy?
    type == TYPE_POST_DEPLOY
  end

  def background_batch?
    type == TYPE_BACKGROUND_BATCH
  end

  def concurrent_only?
    important_queries.all?(&:concurrent?)
  end

  def time_guidance
    return REGULAR_MIGRATION_GUIDANCE_SECONDS if regular?

    if post_deploy?
      return POST_DEPLOY_CONCURRENT_MIGRATION_GUIDANCE_SECONDS if concurrent_only?

      return POST_DEPLOY_MIGRATION_GUIDANCE_SECONDS
    end

    raise 'Unknown migration type'
  end

  def sre_should_be_informed?
    walltime > SRE_NOTIFICATION_GUIDANCE
  end

  def exceeds_time_guidance?
    walltime > time_guidance
  end

  def important_queries
    @important_queries ||= queries.reject(&:excluded?)
  end

  def important_query_executions
    query_executions.reject(&:excluded?)
  end

  def queries_with_warnings
    @queries_with_warnings ||= important_queries.select(&:exceeds_time_guidance?)
  end

  def has_queries_with_warnings?
    queries_with_warnings.any?
  end

  def create_table_queries
    important_queries.select(&:create_table?)
  end

  def create_index_queries
    important_queries.select(&:create_index?)
  end

  def name_formatted
    "#{version} - #{name}"
  end

  def walltime_minutes
    "#{walltime.in_minutes.round(2)}min"
  end

  def time_guidance_minutes
    "#{time_guidance.in_minutes.round(2)}min"
  end

  def warnings
    warnings = queries_with_warnings.map { |q| q.warning(name_formatted) }

    warnings << "#{name_formatted} did not complete successfully, check the job log for details" unless success?

    if sre_should_be_informed?
      warnings << "#{name_formatted} took #{walltime_minutes}. Please add a comment that mentions Release " \
                  "Managers (`@gitlab-org/release/managers`) so they are informed."
    end

    warnings << time_remedy if exceeds_time_guidance?

    if bbm_failed_finalze?
      warnings << "#{name_formatted} attempted to finalize a batched background migration that is still running."
    end

    warnings
  end

  def time_remedy
    if regular? && walltime < POST_DEPLOY_MIGRATION_GUIDANCE_SECONDS
      "#{name_formatted} may need a [post-deploy migration](#{POST_DEPLOY_MIGRATION_GUIDE}) " \
        "to comply with [timing guidelines](#{TIMING_GUIDELINES}). It took #{walltime_minutes}, " \
        "but should not exceed #{time_guidance_minutes}"
    else
      "#{name_formatted} may need a [batched background migration](#{BATCHED_BACKGROUND_MIGRATION_GUIDE}) to " \
        "comply with [timing guidelines](#{TIMING_GUIDELINES}). It took #{walltime_minutes}, " \
        "but should not exceed #{time_guidance_minutes}"
    end
  end

  def warning?
    !success? || exceeds_time_guidance? || has_queries_with_warnings?
  end

  def bbm_failed_finalze?
    !success? &&
      error_message.present? &&
      error_message.include?('Cannot finalize a batched background migration that is still running')
  end

  def init_stats(stats)
    unless stats
      @was_run = false
      return
    end

    @was_run = true
    @total_database_size_change = stats['total_database_size_change']
    @walltime = stats['walltime'].seconds
    @success = stats['success']
    @statistics = stats
    @error_message = stats['error_message']
    init_queries(stats)
  end

  def init_queries(stats)
    query_statistics = stats['query_statistics'] || []
    @queries = query_statistics.map do |query|
      Query.new(query, execution_context: self)
    end
  end

  def sort_key
    [regular? ? 0 : 1, version]
  end
end
