# frozen_string_literal: true

class BackgroundMigration
  MIGRATION_DETAILS_FILE = 'details.json'
  BATCH_DETAILS_FILE = 'batch-details.json'

  def self.from_directory(directory)
    migration_details = JSON.parse(File.read(directory.join(MIGRATION_DETAILS_FILE)))

    attributes = {
      name: directory.basename,
      interval: migration_details['interval'],
      total_tuple_count: migration_details['total_tuple_count'],
      max_batch_size: migration_details['max_batch_size'],
      execution_details: [],
      batches: []
    }

    directory.children.select(&:directory?).each do |batch_dir|
      attributes[:batches] << Migration.background_migration_batch_from_directory(batch_dir)
      attributes[:execution_details] << JSON.parse(File.read(batch_dir.join(BATCH_DETAILS_FILE)))
    end

    new(**attributes)
  end

  attr_reader :name, :batches, :interval, :total_tuple_count, :max_batch_size, :execution_details

  # @note Depending on the type of migration sampled, total_tuple_count and max_batch_size can be nil
  def initialize(name:, batches:, interval:, total_tuple_count:, max_batch_size:, execution_details:)
    @name = name
    @batches = batches
    @interval = interval
    @total_tuple_count = total_tuple_count.to_i
    @max_batch_size = max_batch_size.to_i
    @execution_details = execution_details
  end

  def queries
    matching_queries = batches.flat_map(&:queries).group_by { |q| PgQuery.fingerprint(q.query) }

    matching_queries.map do |query_fingerprint, queries|
      # Each query here comes from pg_stat_statements, so it has already been aggregated for a single batch
      # Aggregate again to get a view of what pg_stat_statements might look like if all batches were run
      # without clearing it.

      query_hash = collapse_in_statements(queries.first.query)

      calls = queries.sum(&:calls)
      total_time = queries.sum(&:total_time)
      max_time = queries.map(&:max_time).max
      mean_time = queries.sum { |q| q.mean_time * q.calls } / calls
      rows = queries.sum(&:rows)
      query_data = {
        query: query_hash[:statement],
        calls: calls,
        total_time: total_time,
        max_time: max_time,
        mean_time: mean_time,
        rows: rows,
        fingerprint: query_fingerprint,
        collapsed: query_hash[:collapsed]
      }.stringify_keys
      Query.new(query_data)
    end
  end

  def time_to_complete
    ActiveSupport::Duration.build(estimated_seconds_to_complete).inspect
  end

  def estimation_info
    [
      "Interval: #{interval}s",
      "Max batch size: #{max_batch_size}",
      "Estimated seconds to complete: #{estimated_seconds_to_complete}s",
      "Average batch time: #{average_time.round(2)}s",
      "Batch size: #{batch_size}",
      "N. of batches sampled: #{batches_sampled}",
      "N. of failed batches: #{batches_failed.size}"
    ].tap do |info|
      info << "Failed batches: #{names_of_batches_failed}" if batches_failed.present?
    end
  end

  def success?
    batches.present? && batches.all?(&:success?)
  end

  private

  def collapse_in_statements(st)
    stmt = PgQuery.parse(st)
    collapsed = false
    stmt.walk! do |node|
      next unless node['kind'] == :AEXPR_IN

      items = node&.rexpr&.list&.items
      next unless items.length > 1

      collapsed = true
      items.clear
      items << PgQuery::Node.new(
        a_const: PgQuery::A_Const.new(
          sval: PgQuery::String.new(sval: 'collapsed_params')
        )
      )
    end

    {
      statement: stmt.deparse,
      collapsed: collapsed
    }
  end

  def estimated_seconds_to_complete
    ([average_time, interval.to_i].max * estimated_number_of_batches).to_i
  end

  def estimated_number_of_batches
    return 0 if batch_size.zero?

    @estimated_number_of_batches ||= total_tuple_count / batch_size
  end

  def average_time
    return 0 if batches_sampled.zero?

    @average_time ||= execution_details.sum { |batch_details| batch_details['time_spent'] } / batches_sampled
  end

  def batch_size
    @batch_size ||= execution_details&.first&.fetch('batch_size', 0).to_i
  end

  def batches_sampled
    @batches_sampled ||= execution_details&.size.to_i
  end

  def batches_failed
    @batches_failed ||= batches.select { |batch| !batch.success? }
  end

  def names_of_batches_failed
    batches_failed.map { |b| b.name || b.statistics['name'] }.join(',')
  end
end
