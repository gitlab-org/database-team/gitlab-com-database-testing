# frozen_string_literal: true

class UnfinalizedBatchedBackgroundMigration
  def self.message(migration)
    <<~MARKDOWN
      Please [finalize](https://docs.gitlab.com/ee/development/database/batched_background_migrations.html#finalize-a-batched-background-migration) #{migration['migration_job_name']} batched background migration.

      #### Note:
      - If the batched background migration is already finalized please update the dictionary.
      - Alternately if the migration is not ready to be finalized, please update the finalize_after date in the dictionary.

      _This is an auto-generated comment from [gitlab-com-database-testing/-/merge_requests/172](https://gitlab.com/gitlab-org/database-team/gitlab-com-database-testing/-/merge_requests/172)._

      cc: @gitlab-org/database-team/triage
    MARKDOWN
  end
end
