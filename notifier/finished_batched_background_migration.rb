# frozen_string_literal: true

class FinishedBatchedBackgroundMigration
  def self.message(migration)
    <<~MARKDOWN
      The #{migration['migration_job_name']} batched background migration is completed!

      _This is an auto-generated comment from [gitlab-com-database-testing/-/merge_requests/190](https://gitlab.com/gitlab-org/database-team/gitlab-com-database-testing/-/merge_requests/190)._
    MARKDOWN
  end
end
