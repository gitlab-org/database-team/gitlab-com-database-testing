# frozen_string_literal: true
require_relative 'table_size_feedback'
require_relative 'utils/groups_helper'

class TableSizeService
  FILE_NAME = 'table-sizes-output.json'
  LABEL = 'database::table size'
  CLASSIFICATION_LABELS = {
    over_limit: 'severity::2',
    large: 'severity::3',
    medium: 'severity::4'
  }.freeze

  Issue = Struct.new(:table_metadata) do
    def title
      "Alert - #{table_metadata['table_name']} table size approaching limits"
    end

    def description
      TableSizeFeedback.new(table_metadata).render_table_size_summary
    end
  end

  def initialize(gitlab_api_client, project_path, table_size_path, epic_iid)
    @gitlab_client = gitlab_api_client
    @project_path = project_path
    @table_size_path = table_size_path
    @epic_iid = epic_iid.to_i
  end

  def execute
    table_sizes.each do |table_metadata|
      issue = Issue.new(table_metadata)

      begin
        gitlab_issue = gitlab_client.issues(
          project_path,
          search: search_identifier(table_metadata['identifier']),
          labels: LABEL
        )

        if gitlab_issue.any?
          next if already_closed?(gitlab_issue.first)

          response = edit_issue(gitlab_issue.first, issue.description, table_metadata)

          puts "Issue updated successfully for #{table_metadata['table_name']} " \
          "at https://gitlab.com/gitlab-org/gitlab/-/issues/#{response.iid}\n"
        else
          response = create_issue(issue.title, issue.description, table_metadata)

          puts "Issue created successfully for #{table_metadata['table_name']} " \
          "at https://gitlab.com/gitlab-org/gitlab/-/issues/#{response.iid}\n"
        end
      rescue Net::OpenTimeout => e
        puts e.message
      end
    end
  end

  private

  attr_reader :gitlab_client, :project_path, :table_size_path, :epic_iid

  def already_closed?(gitlab_issue)
    gitlab_issue.state == 'closed'
  end

  def table_sizes
    JSON.parse(File.read(File.join(table_size_path, FILE_NAME)))
  end

  def edit_issue(issue, description, table_metadata)
    gitlab_client.edit_issue(
      project_path,
      issue.iid,
      {
        description: description,
        confidential: true,
        epic_iid: epic_iid,
        labels: labels(table_metadata, issue: issue)
      }
    )
  end

  def create_issue(title, description, table_metadata)
    gitlab_client.create_issue(
      project_path,
      title,
      {
        description: description,
        labels: labels(table_metadata),
        confidential: true,
        epic_iid: epic_iid
      }
    )
  end

  def groups_helper
    @groups_helper ||= Utils::GroupsHelper.new
  end

  def labels(table_metadata, issue: nil)
    existing_labels = issue&.labels

    labels = groups_helper.find_labels_by_categories(*table_metadata['feature_categories'])
    labels << CLASSIFICATION_LABELS[table_metadata['classification'].to_sym]
    labels << LABEL
    labels << existing_labels

    labels.flatten.compact.uniq
  end

  def search_identifier(identifier)
    OpenSSL::Digest::SHA256.hexdigest(identifier)
  end
end
