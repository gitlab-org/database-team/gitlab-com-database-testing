# frozen_string_literal: true

class MigrationFailureNotice
  attr_reader :multi_db_result

  def initialize(multi_db_result)
    @multi_db_result = multi_db_result
  end

  def has_failed_migrations?
    failed_migrations.keys.map { |k| failed_migrations[k].any? }.any?
  end

  def render
    return unless has_failed_migrations?

    erb('migration_failure_notice').result(binding)
  end

  private

  def failed_migrations
    @failed_migrations ||= begin
      failed = {
        migrations: [],
        background_migrations: []
      }
      multi_db_result.per_db_results.each do |db, db_result|
        db_result.migrations.each do |id, migration|
          failed[:migrations] << [db, migration] unless migration.success?
        end
        db_result.background_migrations.each do |background_migration|
          failed[:background_migrations] << [db, background_migration] unless background_migration.success?
        end
      end
      failed
    end
  end

  def erb(template)
    ERB.new(File.read("templates/#{template}.erb"), trim_mode: '<>%')
  end
end
