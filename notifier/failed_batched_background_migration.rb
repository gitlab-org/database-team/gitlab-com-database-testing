# frozen_string_literal: true

class FailedBatchedBackgroundMigration
  def self.message(migration, author_username)
    <<~MARKDOWN
      The status of the #{migration['migration_job_name']} batched background migration is failed.

      _This is an auto-generated comment from [gitlab-com-database-testing/-/merge_requests/177](https://gitlab.com/gitlab-org/database-team/gitlab-com-database-testing/-/merge_requests/177)._

      cc: @#{author_username} @gitlab-org/database-team/triage
    MARKDOWN
  end
end
