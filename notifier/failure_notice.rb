# frozen_string_literal: true
require 'erb'

class FailureNotice
  def self.message
    template = ERB.new(File.read("templates/failure_notice.erb"), trim_mode: '<>%')
    template.result(binding)
  end
end
