# frozen_string_literal: true

require 'open-uri'
require 'json'

module Utils
  class GroupsHelper
    GROUPS_URL = "https://about.gitlab.com/groups.json"

    def initialize
      @data = fetch_data
    end

    def find_em_usernames_by_categories(*categories)
      data.values.each_with_object([]) do |group, result|
        if has_matching_categories?(group, categories)
          managers = group['engineering_managers']
          result.concat(managers) if managers
        end
      end.uniq
    end

    def find_labels_by_categories(*categories)
      data.values.each_with_object([]) do |group, result|
        if has_matching_categories?(group, categories)
          label = group['label']
          result << label if label
        end
      end.uniq
    end

    private

    attr_reader :data

    def has_matching_categories?(group, categories)
      return false unless group['categories']

      (group['categories'] & categories).any?
    end

    def fetch_data
      JSON.parse(URI.open(GROUPS_URL).read)
    rescue StandardError => e
      raise "Failed to fetch groups data: #{e.message}"
    end
  end
end
