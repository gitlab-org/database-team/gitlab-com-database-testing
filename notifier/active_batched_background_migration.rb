# frozen_string_literal: true

class ActiveBatchedBackgroundMigration
  def self.message(migration)
    <<~MARKDOWN
      The #{migration['migration_job_name']} batched background migration is running!

      _This is an auto-generated comment from [gitlab-com-database-testing/-/merge_requests/213](https://gitlab.com/gitlab-org/database-team/gitlab-com-database-testing/-/merge_requests/213)._
    MARKDOWN
  end
end
