#!/usr/bin/env ruby
# frozen_string_literal: true

require 'active_support'
require 'active_support/core_ext/time'
require 'active_support/core_ext/numeric/time'
require 'active_support/core_ext/enumerable'
require 'active_support/core_ext/module/aliasing'
require 'gitlab'
require 'thor'
require 'date'

require_relative 'feedback'
require_relative 'multi_db_feedback'
require_relative 'background_migration'
require_relative 'migration'
require_relative 'query'
require_relative 'result'
require_relative 'multi_db_result'
require_relative 'warnings'
require_relative 'charts/execution_histogram'
require_relative 'new_table'
require_relative 'failure_notice'
require_relative 'schema_validation_feedback'
require_relative 'unfinalized_batched_background_migration'
require_relative 'failed_batched_background_migration'
require_relative 'finished_batched_background_migration'
require_relative 'finalized_batched_background_migration'
require_relative 'active_batched_background_migration'
require_relative 'gitlab_api_client'
require_relative 'batched_background_migration_status'
require_relative 'migration_failure_notice'
require_relative 'table_size_service'
require_relative 'utils/groups_helper'

IDENTIFIABLE_NOTE_TAG = 'gitlab-org/database-team/gitlab-com-database-testing:identifiable-note'
FINISHED_BATCHED_BACKGROUND_MIGRATION = 'background-migration::finished'
FAILED_BACKGROUND_MIGRATION_LABEL = 'background-migration::failed'
FINALIZED_BATCHED_BACKGROUND_MIGRATION = 'background-migration::finalized'
ACTIVE_BATCHED_BACKGROUND_MIGRATION = 'background-migration::active'

class Notifier < Thor
  desc "send STATS MIGRATIONS CLONE_DETAILS", "send feedback back to merge request"
  def send(database_testing_path)
    comment = comment_for(database_testing_path)
    failed_migration_notice = failed_migration_notice_for(database_testing_path)
    File.write(File.join(database_testing_path, 'comment.md'), comment)

    gitlab = gitlab_client

    ignore_errors do # adding the label is optional
      gitlab.update_merge_request(project_path, merge_request_id, add_labels: ['database-testing-automation'])
    end

    # Look for a note to update
    db_testing_notes = gitlab.merge_request_notes(project_path, merge_request_id).auto_paginate.select do |note|
      note.body.include?(IDENTIFIABLE_NOTE_TAG)
    end

    note = db_testing_notes.max_by { |note| Time.parse(note.created_at) }

    if note && note.type != 'DiscussionNote'
      # The latest note has not led to a discussion. Update it.
      gitlab.edit_merge_request_note(project_path, merge_request_id, note.id, comment)

      puts "Updated comment:\n"
    else
      # This is the first note or the latest note has been discussed on the MR.
      # Don't update, create new note instead.
      note = gitlab.create_merge_request_note(project_path, merge_request_id, comment)

      puts "Posted comment to:\n"
    end

    puts "https://gitlab.com/#{project_path}/-/merge_requests/#{merge_request_id}#note_#{note.id}"

    return unless failed_migration_notice.has_failed_migrations?

    notice = gitlab.create_merge_request_note(
      project_path, merge_request_id,
      failed_migration_notice.render
    )
    puts "Posted failed migration notice to:"
    puts "https://gitlab.com/#{project_path}/-/merge_requests/#{merge_request_id}#note_#{notice.id}"
  end

  desc 'handle_failures PATH', 'analyze failure log and post notification to originating MR'
  def handle_failure
    gitlab = gitlab_client
    note = gitlab.create_merge_request_note(project_path, merge_request_id, FailureNotice.message)
    puts "https://gitlab.com/#{project_path}/-/merge_requests/#{merge_request_id}#note_#{note.id}"
  end

  desc "print STATS MIGRATIONS", "only print feedback"
  def print(database_testing_path)
    puts comment_for(database_testing_path)
  end

  desc "schema validation", "populates the schema validation issue"
  def schema_validation(schema_validation_path)
    gitlab = gitlab_client

    issue_description = SchemaValidationFeedback.new(schema_validation_path).render_schema_validation_summary

    gitlab.edit_issue(project_path, schema_validation_issue_id, { description: issue_description })
  end

  desc "postgres table size", "creates or updates gitlab issue alerting when table size is > 25 GB"
  def table_size(table_size_path)
    TableSizeService.new(gitlab_client, project_path, table_size_path, table_size_epic_iid).execute
  end

  desc "unfinalized batched background migration checker", "finds unfinalized batched background migrations"
  def unfinalized_batched_background_migration_checker(artifacts_path)
    gitlab = gitlab_client

    batched_background_migrations = read_output(artifacts_path, 'unfinalized_migrations_output.json')

    batched_background_migrations.each do |migration|
      finalize_after = migration['finalize_after']
      merge_request_id = migration['merge_request_id']

      next unless finalize_after && Date.parse(finalize_after) < Date.today && migration['finalized_by'].nil?

      warning_message = UnfinalizedBatchedBackgroundMigration.message(migration)

      gitlab.create_merge_request_note(project_path, merge_request_id, warning_message)
    end
  end

  desc "failed batched background migration checker", "finds failed batched background migrations"
  def failed_batched_background_migration_checker(artifacts_path)
    migrations = read_output(artifacts_path, 'failed_migrations_output.json')

    batched_migration_status.notify(
      migrations, FAILED_BACKGROUND_MIGRATION_LABEL, FailedBatchedBackgroundMigration
    )
  end

  desc "finished batched background migration checker", "finds finished batched background migrations"
  def finished_batched_background_migration_checker(artifacts_path)
    migrations = read_output(artifacts_path, 'finished_migrations_output.json')

    batched_migration_status.notify(
      migrations, FINISHED_BATCHED_BACKGROUND_MIGRATION, FinishedBatchedBackgroundMigration
    )
  end

  desc "finalized batched background migration checker", "finds finalized batched background migrations"
  def finalized_batched_background_migration_checker(artifacts_path)
    migrations = read_output(artifacts_path, 'finalized_migrations_output.json')

    batched_migration_status.notify(
      migrations, FINALIZED_BATCHED_BACKGROUND_MIGRATION, FinalizedBatchedBackgroundMigration
    )
  end

  desc "active batched background migration checker", "finds active batched background migrations"
  def active_batched_background_migration_checker(artifacts_path)
    migrations = read_output(artifacts_path, 'active_migrations_output.json')

    batched_migration_status.notify(
      migrations, ACTIVE_BATCHED_BACKGROUND_MIGRATION, ActiveBatchedBackgroundMigration
    )
  end

  private

  def batched_migration_status
    @batched_migration_status ||= BatchedBackgroundMigrationStatus.new(gitlab_client, project_path)
  end

  def read_output(directory_path, file_name)
    file_path = File.join(directory_path, file_name)

    JSON.parse(File.read(file_path))
  end

  # If the CI was triggered by the +gitlab-com-database-testing+ itself, we should use itself ID instead of the
  # TOP_UPSTREAM_SOURCE_PROJECT, which by default is +gitlab-org/gitlab+
  def project_path
    return ENV['CI_PROJECT_ID'] if ENV['CI_SELF_TRIGGERED'] && !ENV['CI_SELF_TRIGGERED'].empty?

    ENV['TOP_UPSTREAM_SOURCE_PROJECT'] || raise("Project path missing: Specify TOP_UPSTREAM_SOURCE_PROJECT")
  end

  def merge_request_id
    ENV['TOP_UPSTREAM_MERGE_REQUEST_IID'] ||
      raise("Upstream merge request id missing: Specify TOP_UPSTREAM_MERGE_REQUEST_IID")
  end

  def schema_validation_issue_id
    ENV['SCHEMA_VALIDATION_ISSUE_ID'] || raise("Schema validation issue id missing: Specify SCHEMA_VALIDATION_ISSUE_ID")
  end

  def table_size_epic_iid
    ENV['TABLE_SIZE_EPIC_IID'] || raise("Table size epic_iid missing: Specify TABLE_SIZE_EPIC_IID")
  end

  def gitlab_client
    GitlabApiClient.new('https://gitlab.com/api/v4').call
  end

  def failed_migration_notice_for(database_testing_path)
    MigrationFailureNotice.new(multi_db_result(database_testing_path))
  end

  def comment_for(database_testing_path)
    contents = feedback_for(database_testing_path).render

    contents += ERB.new(File.read("templates/brought_by.erb"), trim_mode: '<>%').result(binding)
    contents
  end

  def multi_db_result(database_testing_path)
    @multi_db_results ||= {}
    @multi_db_results.fetch(database_testing_path) do
      @multi_db_results[database_testing_path] = MultiDbResult.from_directory(database_testing_path)
    end
  end

  def feedback_for(database_testing_path)
    result = multi_db_result(database_testing_path)

    MultiDbFeedback.new(result)
  end

  def ignore_errors
    yield
  rescue Gitlab::Error::Error => e
    puts "Ignoring the following error: #{e}"
  rescue StandardError => e
    puts "Ignoring the following error: #{e}"
  end
end

Notifier.start(ARGV)
