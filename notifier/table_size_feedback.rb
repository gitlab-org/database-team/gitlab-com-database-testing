# frozen_string_literal: true

require 'erb'

class TableSizeFeedback
  def initialize(table_metadata)
    @table_metadata = table_metadata
  end

  def render_table_size_summary
    b = binding
    b.local_variable_set(:table_metadata, table_metadata)
    b.local_variable_set(:limitations_url, limitations_url)
    b.local_variable_set(:retentions_url, retentions_url)
    b.local_variable_set(:blueprint_url, blueprint_url)
    b.local_variable_set(:slack_url, slack_url)
    erb('table_sizes_table').result(b)
  end

  private

  def erb(template)
    ERB.new(File.read("templates/#{template}.erb"), trim_mode: '<>%')
  end

  def limitations_url
    "https://docs.gitlab.com/ee/development/database/large_tables_limitations.html"
  end

  def retentions_url
    "https://docs.gitlab.com/development/data_retention_policies.html"
  end

  def blueprint_url
    "https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/database_size_limits"
  end

  def slack_url
    "https://gitlab.enterprise.slack.com/archives/C08GQHWGM0S"
  end

  attr_reader :file_path, :table_metadata
end
