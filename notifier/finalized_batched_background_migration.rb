# frozen_string_literal: true

class FinalizedBatchedBackgroundMigration
  def self.message(migration)
    <<~MARKDOWN
      The #{migration['migration_job_name']} batched background migration is finalized!

      _This is an auto-generated comment from [gitlab-com-database-testing/-/merge_requests/201](https://gitlab.com/gitlab-org/database-team/gitlab-com-database-testing/-/merge_requests/201)._
    MARKDOWN
  end
end
