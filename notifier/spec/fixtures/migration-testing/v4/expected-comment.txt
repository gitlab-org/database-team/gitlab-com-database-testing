  <!-- gitlab-org/database-team/gitlab-com-database-testing:identifiable-note -->
### Database migrations (on the main database)

|           | 8 Warnings |
| --------- | -------------------- |
| :warning: |  20210602144718 - CreateTestTable had a query that [exceeded timing guidelines](https://docs.gitlab.com/ee/development/database/query_performance.html#timing-guidelines-for-queries). Run time should not<br /> exceed 100ms, but it was 192.8ms. Please consider possible options to improve the query performance.<br /> <br/><pre>CREATE TABLE "test_tables" ("id" bigserial primary key, "stars" bigint DEFAULT 0 NOT NULL,<br /> "created_at" timestamp(6) NOT NULL, "updated_at" timestamp(6) NOT NULL, "title" text, "notes"<br /> text)</pre> |
| :warning: |  20210603233011 - RegularMigrationWithFiveSecondQuery had a query that [exceeded timing guidelines](https://docs.gitlab.com/ee/development/database/query_performance.html#timing-guidelines-for-queries).<br /> Run time should not exceed 100ms, but it was 5005.1ms. Please consider possible options to improve<br /> the query performance. <br/><pre>SELECT pg_sleep($1)</pre> |
| :warning: |  20221012000000 - FinalizeTestBatchedBackgroundMigrationMain did not complete successfully, check the<br /> job log for details |
| :warning: |  20221012000000 - FinalizeTestBatchedBackgroundMigrationMain attempted to finalize a batched<br /> background migration that is still running. |
| :warning: |  20221012000001 - CreateLongRunningSyncIndex had a query that [exceeded timing guidelines](https://docs.gitlab.com/ee/development/database/query_performance.html#timing-guidelines-for-queries). Run time<br /> should not exceed 300000ms, but it was 5377900.06ms. Please consider possible options to improve the<br /> query performance. <br/><pre>CREATE INDEX CONCURRENTLY "tmp_index_for_null_member_namespace_id" ON<br /> "members" ("member_namespace_id")<br />WHERE member_namespace_id IS NULL</pre> |
| :warning: |  20221012000001 - CreateLongRunningSyncIndex took 93.33min. Please add a comment that mentions<br /> Release Managers (`@gitlab-org/release/managers`) so they are informed. |
| :warning: |  20221012000001 - CreateLongRunningSyncIndex may need a [batched background migration](https://docs.gitlab.com/ee/development/database/batched_background_migrations.html) to comply with<br /> [timing guidelines](https://docs.gitlab.com/ee/development/migration_style_guide.html#how-long-a-migration-should-take). It took 93.33min, but should not exceed 3.0min |
| :warning: |  20990604233157 - MigrationThrowsException did not complete successfully, check the job log for<br /> details |

Migrations included in this change have been executed on gitlab.com data for testing purposes. For details, please see the [migration testing pipeline](https://gitlab.com/gitlab-org/database-team/gitlab-com-database-testing/-/pipelines/4711) (limited access).

| Migration | Type | Total runtime | Result | DB size change |
| --------- | ---- | ------------- | ------ | -------------- |
| 20210602144718 - CreateTestTable | Regular | 2.2 s | :warning: | +24.00 KiB |
| 20210603233011 - RegularMigrationWithFiveSecondQuery | Regular | 6.5 s | :warning: | +0.00 B |
| 20210915152743 - MigrationInheritsGitlabDatabaseMigration | Regular | 1.2 s | :white_check_mark: | +0.00 B |
| 20220223163519 - EnsureGitlabComInMigrations | Regular | 1.2 s | :white_check_mark: | +0.00 B |
| 20220318174439 - QueueTestBackgroundMigration | Regular | 1.8 s | :white_check_mark: | +0.00 B |
| 20221012000000 - FinalizeTestBatchedBackgroundMigrationMain | Regular | 1.4 s | :boom: | +0.00 B |
| 20221012000001 - CreateLongRunningSyncIndex | Regular | 5600.3 s | :warning: | +32.00 KiB |
| 20221130142531 - CreateEfficientlyOrderedColumnsTable | Regular | 1.2 s | :white_check_mark: | +32.00 KiB |
| 20221130142617 - CreateInefficientlyOrderedColumnsTable | Regular | 1.2 s | :white_check_mark: | +40.00 KiB |
| 20210604232017 - DropTestTable | Post deploy | 1.3 s | :white_check_mark: | -24.00 KiB |
| 20990604233157 - MigrationThrowsException | Post deploy | 1.1 s | :boom: | +0.00 B |

<details>
<summary>Runtime Histogram for all migrations</summary>

| Query Runtime | Count |
|---------------|-------|
|0 seconds - 0.01 seconds | 0 |
|0.01 seconds - 0.1 seconds | 8 |
|0.1 seconds - 1 second | 1 |
|1 second - 5 seconds | 0 |
|5 seconds - 15 seconds | 1 |
|15 seconds - 5 minutes | 0 |
|5 minutes + | 1 |

</details>


<details>
<summary> :warning: <h4>Migration: 20210602144718 - CreateTestTable</h4></summary>
* Type: Regular <br>
* Duration: 2.2 s <br>
* Database size change: +24.00 KiB

| Calls | Total Time | Max Time | Mean Time | Rows | Query |
| ----- | ---------- | -------- | --------- | ---- | ----- |
| 1 | 192.8 ms | 192.8 ms | 192.8 ms | 0 | <pre>CREATE TABLE "test_tables" ("id" bigserial primary key, "stars" bigint DEFAULT 0 NOT NULL, "created_at" timestamp(6) NOT NULL, "updated_at" timestamp(6) NOT NULL, "title" text, "notes" text)</pre> |
| 1 | 6.6 ms | 6.6 ms | 6.6 ms | 0 | <pre>ALTER TABLE "test_tables" ADD CONSTRAINT "check_0770ba173a" CHECK (char_length("title") <= 128), ADD CONSTRAINT "check_9cfc473dbc" CHECK (char_length("notes") <= 1024)</pre> |
| 1 | 0.0 ms | 0.0 ms | 0.0 ms | 1 | <pre>SELECT $1::regtype::oid</pre> |

  

<details>
<summary>Histogram for CreateTestTable</summary>

| Query Runtime | Count |
|---------------|-------|
|0 seconds - 0.01 seconds | 0 |
|0.01 seconds - 0.1 seconds | 2 |
|0.1 seconds - 1 second | 1 |
|1 second - 5 seconds | 0 |
|5 seconds - 15 seconds | 0 |
|15 seconds - 5 minutes | 0 |
|5 minutes + | 0 |

</details>


</details><details>
<summary> :warning: <h4>Migration: 20210603233011 - RegularMigrationWithFiveSecondQuery</h4></summary>
* Type: Regular <br>
* Duration: 6.5 s <br>
* Database size change: +0.00 B

| Calls | Total Time | Max Time | Mean Time | Rows | Query |
| ----- | ---------- | -------- | --------- | ---- | ----- |
| 1 | 5005.1 ms | 5005.1 ms | 5005.1 ms | 1 | <pre>SELECT pg_sleep($1)</pre> |


<details>
<summary>Histogram for RegularMigrationWithFiveSecondQuery</summary>

| Query Runtime | Count |
|---------------|-------|
|0 seconds - 0.01 seconds | 0 |
|0.01 seconds - 0.1 seconds | 0 |
|0.1 seconds - 1 second | 0 |
|1 second - 5 seconds | 0 |
|5 seconds - 15 seconds | 1 |
|15 seconds - 5 minutes | 0 |
|5 minutes + | 0 |

</details>


</details><details>
<summary>  <h4>Migration: 20210915152743 - MigrationInheritsGitlabDatabaseMigration</h4></summary>
* Type: Regular <br>
* Duration: 1.2 s <br>
* Database size change: +0.00 B



<i>No histogram available for visualization</i>


</details><details>
<summary>  <h4>Migration: 20220223163519 - EnsureGitlabComInMigrations</h4></summary>
* Type: Regular <br>
* Duration: 1.2 s <br>
* Database size change: +0.00 B



<i>No histogram available for visualization</i>


</details><details>
<summary>  <h4>Migration: 20220318174439 - QueueTestBackgroundMigration</h4></summary>
* Type: Regular <br>
* Duration: 1.8 s <br>
* Database size change: +0.00 B



<i>No histogram available for visualization</i>


</details><details>
<summary> :boom: <h4>Migration: 20221012000000 - FinalizeTestBatchedBackgroundMigrationMain</h4></summary>
* Type: Regular <br>
* Duration: 1.4 s <br>
* Database size change: +0.00 B

| Calls | Total Time | Max Time | Mean Time | Rows | Query |
| ----- | ---------- | -------- | --------- | ---- | ----- |
| 1 | 0.1 ms | 0.1 ms | 0.1 ms | 1 | <pre>SELECT "batched_background_migrations".*<br />  FROM "batched_background_migrations"  WHERE "batched_background_migrations"."job_class_name" = $1 AND "batched_background_migrations"."table_name" = $2 AND "batched_background_migrations"."column_name" = $3 AND (job_arguments = $4) AND "batched_background_migrations"."gitlab_schema" = $5<br />  ORDER BY "batched_background_migrations"."id" ASC<br />  LIMIT $6</pre> |
| 2 | 0.0 ms | 0.0 ms | 0.0 ms | 2 | <pre>SELECT pg_backend_pid()</pre> |


<details>
<summary>Histogram for FinalizeTestBatchedBackgroundMigrationMain</summary>

| Query Runtime | Count |
|---------------|-------|
|0 seconds - 0.01 seconds | 0 |
|0.01 seconds - 0.1 seconds | 3 |
|0.1 seconds - 1 second | 0 |
|1 second - 5 seconds | 0 |
|5 seconds - 15 seconds | 0 |
|15 seconds - 5 minutes | 0 |
|5 minutes + | 0 |

</details>


</details><details>
<summary> :warning: <h4>Migration: 20221012000001 - CreateLongRunningSyncIndex</h4></summary>
* Type: Regular <br>
* Duration: 5600.3 s <br>
* Database size change: +32.00 KiB

| Calls | Total Time | Max Time | Mean Time | Rows | Query |
| ----- | ---------- | -------- | --------- | ---- | ----- |
| 1 | 5377900.1 ms | 5377900.1 ms | 5377900.1 ms | 0 | <pre>CREATE INDEX CONCURRENTLY "tmp_index_for_null_member_namespace_id" ON "members" ("member_namespace_id")<br />WHERE member_namespace_id IS NULL</pre> |

  #### :warning: Index creation timing exceeded

This index creation query might have caused your migration to exceed timing guidelines.
Please examine the timing data on this query and consider if the [asynchronous index creation](https://docs.gitlab.com/ee/development/database/adding_database_indexes.html#create-indexes-asynchronously) process is appropriate.


<details>
<summary>Histogram for CreateLongRunningSyncIndex</summary>

| Query Runtime | Count |
|---------------|-------|
|0 seconds - 0.01 seconds | 0 |
|0.01 seconds - 0.1 seconds | 0 |
|0.1 seconds - 1 second | 0 |
|1 second - 5 seconds | 0 |
|5 seconds - 15 seconds | 0 |
|15 seconds - 5 minutes | 0 |
|5 minutes + | 1 |

</details>


</details><details>
<summary>  <h4>Migration: 20221130142531 - CreateEfficientlyOrderedColumnsTable</h4></summary>
* Type: Regular <br>
* Duration: 1.2 s <br>
* Database size change: +32.00 KiB

| Calls | Total Time | Max Time | Mean Time | Rows | Query |
| ----- | ---------- | -------- | --------- | ---- | ----- |
| 1 | 9.4 ms | 9.4 ms | 9.4 ms | 0 | <pre>CREATE TABLE "efficiently_ordered_columns_tables" ("line_attr" line, "circle_attr" circle, "uuid_attr" uuid, "point_attr" point, "id" bigserial primary key, "bigserial_attr" bigserial, "bigint_attr" bigint, "float_attr" float, "time_attr" time, "timestamp_attr" timestamp, "date_attr" date, "integer_attr" integer, "oid_attr" oid, "smallint_attr" smallint, "boolean_attr" boolean, "inet_attr" inet, "jsonb_attr" jsonb, "text_attr" text)</pre> |

  

<details>
<summary>Histogram for CreateEfficientlyOrderedColumnsTable</summary>

| Query Runtime | Count |
|---------------|-------|
|0 seconds - 0.01 seconds | 0 |
|0.01 seconds - 0.1 seconds | 1 |
|0.1 seconds - 1 second | 0 |
|1 second - 5 seconds | 0 |
|5 seconds - 15 seconds | 0 |
|15 seconds - 5 minutes | 0 |
|5 minutes + | 0 |

</details>


</details><details>
<summary>  <h4>Migration: 20221130142617 - CreateInefficientlyOrderedColumnsTable</h4></summary>
* Type: Regular <br>
* Duration: 1.2 s <br>
* Database size change: +40.00 KiB

| Calls | Total Time | Max Time | Mean Time | Rows | Query |
| ----- | ---------- | -------- | --------- | ---- | ----- |
| 1 | 5.7 ms | 5.7 ms | 5.7 ms | 0 | <pre>CREATE TABLE "inefficiently_ordered_columns_tables" ("id" bigserial primary key, "text_attr" text, "boolean_attr" boolean, "smallint_attr" smallint, "integer_attr" integer, "point_attr" point, "circle_attr" circle, "line_attr" line)</pre> |

  <summary> :warning: Column ordering suggestions</summary>

<i>Ordering columns efficiently (as in below tables) will help us save more space, further details can be found <a href="https://docs.gitlab.com/ee/development/database/ordering_table_columns.html">here</a>.</i>


<pre> CREATE TABLE "inefficiently_ordered_columns_tables" ("id" bigserial primary key, "text_attr" text, "boolean_attr" boolean, "smallint_attr" smallint, "integer_attr" integer, "point_attr" point, "circle_attr" circle, "line_attr" line) </pre>

| Column name | Data Type |
| ----------- | --------- |
| line_attr | line |
| circle_attr | circle |
| point_attr | point |
| id | bigserial |
| integer_attr | int4 |
| smallint_attr | int2 |
| boolean_attr | bool |
| text_attr | text |


<details>
<summary>Histogram for CreateInefficientlyOrderedColumnsTable</summary>

| Query Runtime | Count |
|---------------|-------|
|0 seconds - 0.01 seconds | 0 |
|0.01 seconds - 0.1 seconds | 1 |
|0.1 seconds - 1 second | 0 |
|1 second - 5 seconds | 0 |
|5 seconds - 15 seconds | 0 |
|15 seconds - 5 minutes | 0 |
|5 minutes + | 0 |

</details>


</details><details>
<summary>  <h4>Migration: 20210604232017 - DropTestTable</h4></summary>
* Type: Post deploy <br>
* Duration: 1.3 s <br>
* Database size change: -24.00 KiB

| Calls | Total Time | Max Time | Mean Time | Rows | Query |
| ----- | ---------- | -------- | --------- | ---- | ----- |
| 1 | 2.6 ms | 2.6 ms | 2.6 ms | 0 | <pre>DROP TABLE "test_tables"</pre> |


<details>
<summary>Histogram for DropTestTable</summary>

| Query Runtime | Count |
|---------------|-------|
|0 seconds - 0.01 seconds | 0 |
|0.01 seconds - 0.1 seconds | 1 |
|0.1 seconds - 1 second | 0 |
|1 second - 5 seconds | 0 |
|5 seconds - 15 seconds | 0 |
|15 seconds - 5 minutes | 0 |
|5 minutes + | 0 |

</details>


</details><details>
<summary> :boom: <h4>Migration: 20990604233157 - MigrationThrowsException</h4></summary>
* Type: Post deploy <br>
* Duration: 1.1 s <br>
* Database size change: +0.00 B



<i>No histogram available for visualization</i>


</details>

<details>
<summary> :boom: <h4>Background Migration: TestBackgroundMigration</h4></summary>

<details>
<summary>Sampled 6 batches. Estimated Time to complete: 1 hour and 36 minutes</summary>

  - Interval: 120s
  - Max batch size: 750
  - Estimated seconds to complete: 5760s
  - Average batch time: 1.08s
  - Batch size: 150
  - N. of batches sampled: 6
  - N. of failed batches: 1
  - Failed batches: batch_5

  Time estimation is conservative and based on sampling production data in a test environment. It represents the max time that migration could take. The actual time may differ from this estimation.
</details>

<small>_Consider changing max_batch_size and interval if this estimate is unacceptable._</small>


| Calls | Total Time | Max Time | Mean Time | Rows | Collapsed |Query |
| ----- | ---------- | -------- | --------- | ---- | --------- |----- |
| 5 | 15012.1 ms | 5004.0 ms | 3002.4 ms | 5 | No | <pre>SELECT pg_sleep($1)</pre> |
| 5 | 901.4 ms | 616.5 ms | 180.3 ms | 72 | Yes | <pre>UPDATE sent_notifications<br />  SET recipient_id = $1<br />  WHERE sent_notifications.id IN ($2)</pre> |
| 2 | 34.4 ms | 34.1 ms | 17.2 ms | 2 | No | <pre>INSERT INTO batched_background_migration_job_transition_logs (batched_background_migration_job_id, created_at, updated_at, previous_status, next_status) VALUES ($1, $2, $3, $4, $5) RETURNING id</pre> |
| 1 | 6.9 ms | 6.9 ms | 6.9 ms | 1 | No | <pre>UPDATE batched_background_migration_jobs<br />  SET updated_at = $1, finished_at = $2, status = $3, metrics = $4<br />  WHERE batched_background_migration_jobs.id = $5</pre> |
| 4 | 7.5 ms | 5.0 ms | 1.9 ms | 80 | No | <pre>SELECT sent_notifications.*<br />  FROM sent_notifications<br />  WHERE sent_notifications.id BETWEEN $1 AND $2 AND sent_notifications.id >= $3 AND sent_notifications.id < $4</pre> |
| 1 | 3.9 ms | 3.9 ms | 3.9 ms | 1 | No | <pre>UPDATE batched_background_migration_jobs<br />  SET updated_at = $1, started_at = $2, status = $3, attempts = $4<br />  WHERE batched_background_migration_jobs.id = $5</pre> |
| 5 | 0.2 ms | 0.1 ms | 0.0 ms | 4 | No | <pre>SELECT sent_notifications.id<br />  FROM sent_notifications<br />  WHERE sent_notifications.id BETWEEN $1 AND $2 AND sent_notifications.id >= $3<br />  ORDER BY sent_notifications.id ASC<br />  LIMIT $4<br />  OFFSET $5</pre> |
| 2 | 0.1 ms | 0.1 ms | 0.1 ms | 2 | No | <pre>SELECT batched_background_migration_jobs.*<br />  FROM batched_background_migration_jobs<br />  WHERE batched_background_migration_jobs.id = $1<br />  LIMIT $2</pre> |
| 1 | 0.1 ms | 0.1 ms | 0.1 ms | 1 | No | <pre>SELECT sum(batched_background_migration_jobs.batch_size)<br />  FROM batched_background_migration_jobs<br />  WHERE batched_background_migration_jobs.batched_background_migration_id = $1 AND batched_background_migration_jobs.status IN ($2)</pre> |
| 1 | 0.0 ms | 0.0 ms | 0.0 ms | 1 | No | <pre>SELECT batched_background_migrations.*<br />  FROM batched_background_migrations<br />  WHERE batched_background_migrations.id = $1<br />  LIMIT $2</pre> |
| 1 | 0.0 ms | 0.0 ms | 0.0 ms | 20 | No | <pre>SELECT sent_notifications.*<br />  FROM sent_notifications<br />  WHERE sent_notifications.id BETWEEN $1 AND $2 AND sent_notifications.id >= $3</pre> |
| 1 | 0.0 ms | 0.0 ms | 0.0 ms | 1 | No | <pre>SELECT sent_notifications.id<br />  FROM sent_notifications<br />  WHERE sent_notifications.id BETWEEN $1 AND $2<br />  ORDER BY sent_notifications.id ASC<br />  LIMIT $3</pre> |

<details>
<summary>Histogram of batch runtimes for TestBackgroundMigration</summary>

| Batch Runtime | Count |
|---------------|-------|
|0 seconds - 10 seconds | 6 |
|10 seconds - 1 minute | 0 |
|1 minute - 2 minutes | 0 |
|2 minutes - 3 minutes | 0 |
|3 minutes - 5 minutes | 0 |
|5 minutes + | 0 |

</details>

<details>
<summary>Histogram across all sampled batches of TestBackgroundMigration</summary>

| Query Runtime | Count |
|---------------|-------|
|0 seconds - 0.1 seconds | 22 |
|0.1 seconds - 0.5 seconds | 1 |
|0.5 seconds - 1 second | 1 |
|1 second - 2 seconds | 1 |
|2 seconds - 5 seconds | 3 |
|5 seconds + | 1 |

</details>

</details>
#### Other information

<details>
  <summary>Other migrations pending on GitLab.com</summary>

  | Migration | Type | Total runtime | Result | DB size change |
| --------- | ---- | ------------- | ------ | -------------- |
| 20220929081645 - TmpIdxNullMemberNamespaceId | Post deploy | 56.3 s | :white_check_mark: | +32.00 KiB |

</details>

<details>
  <summary>Clone details</summary>

  | Clone ID | Clone Created At | Clone Data Timestamp | Expected Removal Time |
| -------- | ---------------- | -------------------- | --------------------- |
| [`database-testing-1575761-8632787-main`](https://console.postgres.ai/gitlab/gitlab-production-tunnel/instances/59/clones/database-testing-1575761-8632787-main) | 2022-11-30T15:08:09Z | 2022-11-30T12:09:58Z | 2022-12-01 04:07:13 +0000 |
| [`database-testing-1575761-8632787-ci`](https://console.postgres.ai/gitlab/gitlab-production-ci/instances/165/clones/database-testing-1575761-8632787-ci) | 2022-11-30T15:08:09Z | 2022-11-30T12:46:56Z | 2022-12-01 04:07:13 +0000 |

</details>

[Job artifacts](https://gitlab.com/gitlab-org/database-team/gitlab-com-database-testing/-/jobs/1354666720/artifacts/browse/migration-testing/)

<!-- JSON: eyJ2ZXJzaW9uIjoyLCJkYXRhIjpbeyJ2ZXJzaW9uIjp7InRpbWVzdGFtcCI6
MjAyMTA2MDIxNDQ3MTh9LCJ3YWxsdGltZSI6Mi4xOTg1MTQwMjE5MzMwNzg4
LCJ0b3RhbF9kYXRhYmFzZV9zaXplX2NoYW5nZSI6MjQ1NzYsInN1Y2Nlc3Mi
OnRydWV9LHsidmVyc2lvbiI6eyJ0aW1lc3RhbXAiOjIwMjEwNjAzMjMzMDEx
fSwid2FsbHRpbWUiOjYuNTM4Njc0ODM4ODQwOTYxNSwidG90YWxfZGF0YWJh
c2Vfc2l6ZV9jaGFuZ2UiOjAsInN1Y2Nlc3MiOnRydWV9LHsidmVyc2lvbiI6
eyJ0aW1lc3RhbXAiOjIwMjEwOTE1MTUyNzQzfSwid2FsbHRpbWUiOjEuMjE4
NzE5MTYyMDQ2OTA5MywidG90YWxfZGF0YWJhc2Vfc2l6ZV9jaGFuZ2UiOjAs
InN1Y2Nlc3MiOnRydWV9LHsidmVyc2lvbiI6eyJ0aW1lc3RhbXAiOjIwMjIw
MjIzMTYzNTE5fSwid2FsbHRpbWUiOjEuMTg0NDUwMjc2MTk2MDAzLCJ0b3Rh
bF9kYXRhYmFzZV9zaXplX2NoYW5nZSI6MCwic3VjY2VzcyI6dHJ1ZX0seyJ2
ZXJzaW9uIjp7InRpbWVzdGFtcCI6MjAyMjAzMTgxNzQ0Mzl9LCJ3YWxsdGlt
ZSI6MS44MTY4NDE1ODc0MjQyNzgzLCJ0b3RhbF9kYXRhYmFzZV9zaXplX2No
YW5nZSI6MCwic3VjY2VzcyI6dHJ1ZX0seyJ2ZXJzaW9uIjp7InRpbWVzdGFt
cCI6MjAyMjEwMTIwMDAwMDB9LCJ3YWxsdGltZSI6MS40MzgyMjE4MzU1MzEy
OTQzLCJ0b3RhbF9kYXRhYmFzZV9zaXplX2NoYW5nZSI6MCwic3VjY2VzcyI6
ZmFsc2V9LHsidmVyc2lvbiI6eyJ0aW1lc3RhbXAiOjIwMjIxMDEyMDAwMDAx
fSwid2FsbHRpbWUiOjU2MDAuMjYzMjgyOTU0NjkzLCJ0b3RhbF9kYXRhYmFz
ZV9zaXplX2NoYW5nZSI6MzI3NjgsInN1Y2Nlc3MiOnRydWV9LHsidmVyc2lv
biI6eyJ0aW1lc3RhbXAiOjIwMjIxMTMwMTQyNTMxfSwid2FsbHRpbWUiOjEu
MjEwNzE3NTI5MDU4NDU2NCwidG90YWxfZGF0YWJhc2Vfc2l6ZV9jaGFuZ2Ui
OjMyNzY4LCJzdWNjZXNzIjp0cnVlfSx7InZlcnNpb24iOnsidGltZXN0YW1w
IjoyMDIyMTEzMDE0MjYxN30sIndhbGx0aW1lIjoxLjIxODAyMTgxMDA1NDc3
OSwidG90YWxfZGF0YWJhc2Vfc2l6ZV9jaGFuZ2UiOjQwOTYwLCJzdWNjZXNz
Ijp0cnVlfSx7InZlcnNpb24iOnsidGltZXN0YW1wIjoyMDIxMDYwNDIzMjAx
N30sIndhbGx0aW1lIjoxLjI5NDE0MzEyNTQxNDg0ODMsInRvdGFsX2RhdGFi
YXNlX3NpemVfY2hhbmdlIjotMjQ1NzYsInN1Y2Nlc3MiOnRydWV9LHsidmVy
c2lvbiI6eyJ0aW1lc3RhbXAiOjIwOTkwNjA0MjMzMTU3fSwid2FsbHRpbWUi
OjEuMDYwOTI0NTAwMjI2OTc0NSwidG90YWxfZGF0YWJhc2Vfc2l6ZV9jaGFu
Z2UiOjAsInN1Y2Nlc3MiOmZhbHNlfV19
 -->
  <!-- gitlab-org/database-team/gitlab-com-database-testing:identifiable-note -->
### Database migrations (on the ci database)

|           | 3 Warnings |
| --------- | -------------------- |
| :warning: |  20210602144718 - CreateTestTable had a query that [exceeded timing guidelines](https://docs.gitlab.com/ee/development/database/query_performance.html#timing-guidelines-for-queries). Run time should not<br /> exceed 100ms, but it was 122.94ms. Please consider possible options to improve the query<br /> performance. <br/><pre>CREATE TABLE "test_tables" ("id" bigserial primary key, "stars" bigint<br /> DEFAULT 0 NOT NULL, "created_at" timestamp(6) NOT NULL, "updated_at" timestamp(6) NOT NULL, "title"<br /> text, "notes" text)</pre> |
| :warning: |  20210603233011 - RegularMigrationWithFiveSecondQuery had a query that [exceeded timing guidelines](https://docs.gitlab.com/ee/development/database/query_performance.html#timing-guidelines-for-queries).<br /> Run time should not exceed 100ms, but it was 5005.1ms. Please consider possible options to improve<br /> the query performance. <br/><pre>SELECT pg_sleep($1)</pre> |
| :warning: |  20990604233157 - MigrationThrowsException did not complete successfully, check the job log for<br /> details |

Migrations included in this change have been executed on gitlab.com data for testing purposes. For details, please see the [migration testing pipeline](https://gitlab.com/gitlab-org/database-team/gitlab-com-database-testing/-/pipelines/4711) (limited access).

| Migration | Type | Total runtime | Result | DB size change |
| --------- | ---- | ------------- | ------ | -------------- |
| 20210602144718 - CreateTestTable | Regular | 1.9 s | :warning: | +24.00 KiB |
| 20210603233011 - RegularMigrationWithFiveSecondQuery | Regular | 6.2 s | :warning: | +0.00 B |
| 20210915152743 - MigrationInheritsGitlabDatabaseMigration | Regular | 1.2 s | :white_check_mark: | +0.00 B |
| 20220223163519 - EnsureGitlabComInMigrations | Regular | 1.2 s | :white_check_mark: | +0.00 B |
| 20220318174439 - QueueTestBackgroundMigration | Regular | 1.7 s | :white_check_mark: | +0.00 B |
| 20221130142531 - CreateEfficientlyOrderedColumnsTable | Regular | 1.2 s | :white_check_mark: | +40.00 KiB |
| 20221130142617 - CreateInefficientlyOrderedColumnsTable | Regular | 1.2 s | :white_check_mark: | +48.00 KiB |
| 20210604232017 - DropTestTable | Post deploy | 1.4 s | :white_check_mark: | -24.00 KiB |
| 20990604233157 - MigrationThrowsException | Post deploy | 1.1 s | :boom: | +8.00 KiB <sup>[[note](https://docs.gitlab.com/ee/development/database/database_migration_pipeline.html#database-size-increase)]</sup> |

<details>
<summary>Runtime Histogram for all migrations</summary>

| Query Runtime | Count |
|---------------|-------|
|0 seconds - 0.01 seconds | 0 |
|0.01 seconds - 0.1 seconds | 5 |
|0.1 seconds - 1 second | 1 |
|1 second - 5 seconds | 0 |
|5 seconds - 15 seconds | 1 |
|15 seconds - 5 minutes | 0 |
|5 minutes + | 0 |

</details>


<details>
<summary> :warning: <h4>Migration: 20210602144718 - CreateTestTable</h4></summary>
* Type: Regular <br>
* Duration: 1.9 s <br>
* Database size change: +24.00 KiB

| Calls | Total Time | Max Time | Mean Time | Rows | Query |
| ----- | ---------- | -------- | --------- | ---- | ----- |
| 1 | 122.9 ms | 122.9 ms | 122.9 ms | 0 | <pre>CREATE TABLE "test_tables" ("id" bigserial primary key, "stars" bigint DEFAULT 0 NOT NULL, "created_at" timestamp(6) NOT NULL, "updated_at" timestamp(6) NOT NULL, "title" text, "notes" text)</pre> |
| 1 | 8.3 ms | 8.3 ms | 8.3 ms | 0 | <pre>ALTER TABLE "test_tables" ADD CONSTRAINT "check_0770ba173a" CHECK (char_length("title") <= 128), ADD CONSTRAINT "check_9cfc473dbc" CHECK (char_length("notes") <= 1024)</pre> |
| 1 | 0.0 ms | 0.0 ms | 0.0 ms | 1 | <pre>SELECT $1::regtype::oid</pre> |

  

<details>
<summary>Histogram for CreateTestTable</summary>

| Query Runtime | Count |
|---------------|-------|
|0 seconds - 0.01 seconds | 0 |
|0.01 seconds - 0.1 seconds | 2 |
|0.1 seconds - 1 second | 1 |
|1 second - 5 seconds | 0 |
|5 seconds - 15 seconds | 0 |
|15 seconds - 5 minutes | 0 |
|5 minutes + | 0 |

</details>


</details><details>
<summary> :warning: <h4>Migration: 20210603233011 - RegularMigrationWithFiveSecondQuery</h4></summary>
* Type: Regular <br>
* Duration: 6.2 s <br>
* Database size change: +0.00 B

| Calls | Total Time | Max Time | Mean Time | Rows | Query |
| ----- | ---------- | -------- | --------- | ---- | ----- |
| 1 | 5005.1 ms | 5005.1 ms | 5005.1 ms | 1 | <pre>SELECT pg_sleep($1)</pre> |


<details>
<summary>Histogram for RegularMigrationWithFiveSecondQuery</summary>

| Query Runtime | Count |
|---------------|-------|
|0 seconds - 0.01 seconds | 0 |
|0.01 seconds - 0.1 seconds | 0 |
|0.1 seconds - 1 second | 0 |
|1 second - 5 seconds | 0 |
|5 seconds - 15 seconds | 1 |
|15 seconds - 5 minutes | 0 |
|5 minutes + | 0 |

</details>


</details><details>
<summary>  <h4>Migration: 20210915152743 - MigrationInheritsGitlabDatabaseMigration</h4></summary>
* Type: Regular <br>
* Duration: 1.2 s <br>
* Database size change: +0.00 B



<i>No histogram available for visualization</i>


</details><details>
<summary>  <h4>Migration: 20220223163519 - EnsureGitlabComInMigrations</h4></summary>
* Type: Regular <br>
* Duration: 1.2 s <br>
* Database size change: +0.00 B



<i>No histogram available for visualization</i>


</details><details>
<summary>  <h4>Migration: 20220318174439 - QueueTestBackgroundMigration</h4></summary>
* Type: Regular <br>
* Duration: 1.7 s <br>
* Database size change: +0.00 B



<i>No histogram available for visualization</i>


</details><details>
<summary>  <h4>Migration: 20221130142531 - CreateEfficientlyOrderedColumnsTable</h4></summary>
* Type: Regular <br>
* Duration: 1.2 s <br>
* Database size change: +40.00 KiB

| Calls | Total Time | Max Time | Mean Time | Rows | Query |
| ----- | ---------- | -------- | --------- | ---- | ----- |
| 1 | 12.0 ms | 12.0 ms | 12.0 ms | 0 | <pre>CREATE TABLE "efficiently_ordered_columns_tables" ("line_attr" line, "circle_attr" circle, "uuid_attr" uuid, "point_attr" point, "id" bigserial primary key, "bigserial_attr" bigserial, "bigint_attr" bigint, "float_attr" float, "time_attr" time, "timestamp_attr" timestamp, "date_attr" date, "integer_attr" integer, "oid_attr" oid, "smallint_attr" smallint, "boolean_attr" boolean, "inet_attr" inet, "jsonb_attr" jsonb, "text_attr" text)</pre> |

  

<details>
<summary>Histogram for CreateEfficientlyOrderedColumnsTable</summary>

| Query Runtime | Count |
|---------------|-------|
|0 seconds - 0.01 seconds | 0 |
|0.01 seconds - 0.1 seconds | 1 |
|0.1 seconds - 1 second | 0 |
|1 second - 5 seconds | 0 |
|5 seconds - 15 seconds | 0 |
|15 seconds - 5 minutes | 0 |
|5 minutes + | 0 |

</details>


</details><details>
<summary>  <h4>Migration: 20221130142617 - CreateInefficientlyOrderedColumnsTable</h4></summary>
* Type: Regular <br>
* Duration: 1.2 s <br>
* Database size change: +48.00 KiB

| Calls | Total Time | Max Time | Mean Time | Rows | Query |
| ----- | ---------- | -------- | --------- | ---- | ----- |
| 1 | 19.2 ms | 19.2 ms | 19.2 ms | 0 | <pre>CREATE TABLE "inefficiently_ordered_columns_tables" ("id" bigserial primary key, "text_attr" text, "boolean_attr" boolean, "smallint_attr" smallint, "integer_attr" integer, "point_attr" point, "circle_attr" circle, "line_attr" line)</pre> |

  <summary> :warning: Column ordering suggestions</summary>

<i>Ordering columns efficiently (as in below tables) will help us save more space, further details can be found <a href="https://docs.gitlab.com/ee/development/database/ordering_table_columns.html">here</a>.</i>


<pre> CREATE TABLE "inefficiently_ordered_columns_tables" ("id" bigserial primary key, "text_attr" text, "boolean_attr" boolean, "smallint_attr" smallint, "integer_attr" integer, "point_attr" point, "circle_attr" circle, "line_attr" line) </pre>

| Column name | Data Type |
| ----------- | --------- |
| line_attr | line |
| circle_attr | circle |
| point_attr | point |
| id | bigserial |
| integer_attr | int4 |
| smallint_attr | int2 |
| boolean_attr | bool |
| text_attr | text |


<details>
<summary>Histogram for CreateInefficientlyOrderedColumnsTable</summary>

| Query Runtime | Count |
|---------------|-------|
|0 seconds - 0.01 seconds | 0 |
|0.01 seconds - 0.1 seconds | 1 |
|0.1 seconds - 1 second | 0 |
|1 second - 5 seconds | 0 |
|5 seconds - 15 seconds | 0 |
|15 seconds - 5 minutes | 0 |
|5 minutes + | 0 |

</details>


</details><details>
<summary>  <h4>Migration: 20210604232017 - DropTestTable</h4></summary>
* Type: Post deploy <br>
* Duration: 1.4 s <br>
* Database size change: -24.00 KiB

| Calls | Total Time | Max Time | Mean Time | Rows | Query |
| ----- | ---------- | -------- | --------- | ---- | ----- |
| 1 | 6.8 ms | 6.8 ms | 6.8 ms | 0 | <pre>DROP TABLE "test_tables"</pre> |


<details>
<summary>Histogram for DropTestTable</summary>

| Query Runtime | Count |
|---------------|-------|
|0 seconds - 0.01 seconds | 0 |
|0.01 seconds - 0.1 seconds | 1 |
|0.1 seconds - 1 second | 0 |
|1 second - 5 seconds | 0 |
|5 seconds - 15 seconds | 0 |
|15 seconds - 5 minutes | 0 |
|5 minutes + | 0 |

</details>


</details><details>
<summary> :boom: <h4>Migration: 20990604233157 - MigrationThrowsException</h4></summary>
* Type: Post deploy <br>
* Duration: 1.1 s <br>
* Database size change: +8.00 KiB <sup>[[note](https://docs.gitlab.com/ee/development/database/database_migration_pipeline.html#database-size-increase)]</sup>



<i>No histogram available for visualization</i>


</details>

#### Other information

<details>
  <summary>Other migrations pending on GitLab.com</summary>

  | Migration | Type | Total runtime | Result | DB size change |
| --------- | ---- | ------------- | ------ | -------------- |
| 20220929081645 - TmpIdxNullMemberNamespaceId | Post deploy | 112.4 s | :white_check_mark: | +32.00 KiB |

</details>

<details>
  <summary>Clone details</summary>

  | Clone ID | Clone Created At | Clone Data Timestamp | Expected Removal Time |
| -------- | ---------------- | -------------------- | --------------------- |
| [`database-testing-1575761-8632787-main`](https://console.postgres.ai/gitlab/gitlab-production-tunnel/instances/59/clones/database-testing-1575761-8632787-main) | 2022-11-30T15:08:09Z | 2022-11-30T12:09:58Z | 2022-12-01 04:07:13 +0000 |
| [`database-testing-1575761-8632787-ci`](https://console.postgres.ai/gitlab/gitlab-production-ci/instances/165/clones/database-testing-1575761-8632787-ci) | 2022-11-30T15:08:09Z | 2022-11-30T12:46:56Z | 2022-12-01 04:07:13 +0000 |

</details>

[Job artifacts](https://gitlab.com/gitlab-org/database-team/gitlab-com-database-testing/-/jobs/1354666720/artifacts/browse/migration-testing/)

<!-- JSON: eyJ2ZXJzaW9uIjoyLCJkYXRhIjpbeyJ2ZXJzaW9uIjp7InRpbWVzdGFtcCI6
MjAyMTA2MDIxNDQ3MTh9LCJ3YWxsdGltZSI6MS45MTA5NTcxODc0MTQxNjkz
LCJ0b3RhbF9kYXRhYmFzZV9zaXplX2NoYW5nZSI6MjQ1NzYsInN1Y2Nlc3Mi
OnRydWV9LHsidmVyc2lvbiI6eyJ0aW1lc3RhbXAiOjIwMjEwNjAzMjMzMDEx
fSwid2FsbHRpbWUiOjYuMTk3NDIxNjQwMTU3NywidG90YWxfZGF0YWJhc2Vf
c2l6ZV9jaGFuZ2UiOjAsInN1Y2Nlc3MiOnRydWV9LHsidmVyc2lvbiI6eyJ0
aW1lc3RhbXAiOjIwMjEwOTE1MTUyNzQzfSwid2FsbHRpbWUiOjEuMTg5MzU2
NTk1Mjc3Nzg2MywidG90YWxfZGF0YWJhc2Vfc2l6ZV9jaGFuZ2UiOjAsInN1
Y2Nlc3MiOnRydWV9LHsidmVyc2lvbiI6eyJ0aW1lc3RhbXAiOjIwMjIwMjIz
MTYzNTE5fSwid2FsbHRpbWUiOjEuMTUwMDQzMjI2Nzc4NTA3MiwidG90YWxf
ZGF0YWJhc2Vfc2l6ZV9jaGFuZ2UiOjAsInN1Y2Nlc3MiOnRydWV9LHsidmVy
c2lvbiI6eyJ0aW1lc3RhbXAiOjIwMjIwMzE4MTc0NDM5fSwid2FsbHRpbWUi
OjEuNzMzNzc5ODI1MjcwMTc2LCJ0b3RhbF9kYXRhYmFzZV9zaXplX2NoYW5n
ZSI6MCwic3VjY2VzcyI6dHJ1ZX0seyJ2ZXJzaW9uIjp7InRpbWVzdGFtcCI6
MjAyMjExMzAxNDI1MzF9LCJ3YWxsdGltZSI6MS4yMzM1ODA4MDUzNjEyNzEs
InRvdGFsX2RhdGFiYXNlX3NpemVfY2hhbmdlIjo0MDk2MCwic3VjY2VzcyI6
dHJ1ZX0seyJ2ZXJzaW9uIjp7InRpbWVzdGFtcCI6MjAyMjExMzAxNDI2MTd9
LCJ3YWxsdGltZSI6MS4yMzI4NTEzODYwNzAyNTE1LCJ0b3RhbF9kYXRhYmFz
ZV9zaXplX2NoYW5nZSI6NDkxNTIsInN1Y2Nlc3MiOnRydWV9LHsidmVyc2lv
biI6eyJ0aW1lc3RhbXAiOjIwMjEwNjA0MjMyMDE3fSwid2FsbHRpbWUiOjEu
NDEzMzE5Njk5NDY2MjI4NSwidG90YWxfZGF0YWJhc2Vfc2l6ZV9jaGFuZ2Ui
Oi0yNDU3Niwic3VjY2VzcyI6dHJ1ZX0seyJ2ZXJzaW9uIjp7InRpbWVzdGFt
cCI6MjA5OTA2MDQyMzMxNTd9LCJ3YWxsdGltZSI6MS4wNTMxMzgyMzM3MjEy
NTYzLCJ0b3RhbF9kYXRhYmFzZV9zaXplX2NoYW5nZSI6ODE5Miwic3VjY2Vz
cyI6ZmFsc2V9XX0=
 -->
