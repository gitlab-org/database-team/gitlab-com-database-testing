# frozen_string_literal: true
require 'spec_helper'

RSpec.describe TableSizeService do
  let(:project_path) { 'project_path' }
  let(:table_size_path) { 'spec/fixtures' }
  let(:table_metadata) { JSON.parse(File.read(File.join(table_size_path, described_class::FILE_NAME))).first }
  let(:issue_title) { 'Alert - merge_request_diff_commits table size approaching limits' }
  let(:issues) { instance_double('issues') }
  let(:groups_helper) { instance_double(Utils::GroupsHelper) }
  let(:groups_data) { File.read(file_fixture('groups.json')) }
  let(:epic_iid) { 1 }
  let(:gitlab_client) do
    instance_double(
      'client',
      issues: issues,
      create_issue: instance_double('response', iid: 1), edit_issue: instance_double('response', iid: 1)
    )
  end

  subject(:service) { described_class.new(gitlab_client, project_path, table_size_path, epic_iid) }

  before do
    allow(Utils::GroupsHelper).to receive(:new).and_return(groups_helper)
    allow(groups_helper).to receive(:find_labels_by_categories).and_return(['group::import and integrate'])
    allow(groups_helper).to receive(:find_em_usernames_by_categories).and_return('wortschi')
    stub_request(:get, 'https://about.gitlab.com/groups.json').to_return(status: 200, body: groups_data)
  end

  shared_context 'when making gitlab issues request' do |state: nil|
    let(:request_params) do
      {
        project_path: project_path,
        search: issue_title,
        labels: described_class::LABEL
      }.tap { |params| params[:state] = state if state }
    end

    before do
      allow(gitlab_client).to receive(:issues)
        .with(project_path, search: issue_title, labels: described_class::LABEL, **state_param)
        .and_return(gitlab_response)
    end
  end

  describe 'Issue struct' do
    let(:issue) { described_class::Issue.new(table_metadata) }

    it 'generates correct title' do
      expect(issue.title).to eq(issue_title)
    end

    it 'generates description using TableSizeFeedback' do
      expect(issue.description).to include('the rapid growth rate indicates action is required')
    end
  end

  describe '#execute' do
    subject(:execute) { service.execute }

    context 'when issue does not exist' do
      let(:issues) { instance_double('issues', any?: false) }
      let(:gitlab_response) { [] }
      let(:state_param) { {} }

      include_examples 'when making gitlab issues request'

      it 'creates a new issue' do
        expect(gitlab_client).to receive(:create_issue).with(
          project_path,
          issue_title,
          hash_including(
            confidential: true,
            labels: kind_of(Array),
            epic_iid: kind_of(Integer),
            description: kind_of(String)
          )
        )

        execute
      end
    end

    context 'when issue already exists' do
      let(:issue_double) { instance_double('issue', iid: 1, state: 'opened', labels: 'label') }
      let(:issues) { instance_double('issues', any?: true, first: issue_double) }
      let(:gitlab_response) { [issue_double] }
      let(:state_param) { { state: 'opened' } }

      include_examples 'when making gitlab issues request', state: 'opened'

      it 'updates the existing issue' do
        expect(gitlab_client).to receive(:edit_issue).with(
          project_path,
          issue_double.iid,
          hash_including(confidential: true)
        )
        execute
      end
    end

    context 'when closed issue exists' do
      let(:issue_double) { instance_double('issue', iid: 1, state: 'closed') }
      let(:issues) { instance_double('issues', any?: true, first: issue_double) }
      let(:gitlab_response) { issues }
      let(:state_param) { {} }

      include_examples 'when making gitlab issues request'

      it 'skips the closed issue' do
        expect(gitlab_client).not_to receive(:edit_issue)
        expect(gitlab_client).not_to receive(:create_issue)
        execute
      end
    end

    context 'when API request times out' do
      before do
        allow(gitlab_client).to receive(:issues)
          .and_raise(Net::OpenTimeout.new('Connection timed out'))
      end

      it 'handles the timeout gracefully' do
        expect { execute }.not_to raise_error
      end
    end
  end
end
