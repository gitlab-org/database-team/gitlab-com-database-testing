# frozen_string_literal: true
require 'spec_helper'

RSpec.describe Utils::GroupsHelper do
  let(:helper) { described_class.new }
  let(:groups_data) { File.read(file_fixture('groups.json')) }

  before do
    stub_request(:get, described_class::GROUPS_URL).to_return(status: 200, body: groups_data)
  end

  describe '#find_em_usernames_by_categories' do
    subject(:usernames) { helper.find_em_usernames_by_categories(*categories) }

    context 'when request succeeds' do
      context 'when searching for integration categories' do
        let(:categories) { %w[integrations api] }

        it 'returns the correct engineering manager username' do
          expect(usernames).to include('wortschi')
        end
      end

      context 'when searching multiple categories' do
        let(:categories) { %w[design_system navigation] }

        it 'returns engineering managers from all matching groups' do
          expect(usernames).to include('samdbeckham')
        end
      end

      context 'when category has no matching groups' do
        let(:categories) { ['non_existent_category'] }

        it 'returns an empty array' do
          expect(usernames).to be_empty
        end
      end
    end

    context 'when request fails' do
      let(:categories) { ['any_category'] }

      context 'when an error occurs' do
        before do
          stub_request(:get, described_class::GROUPS_URL).to_return(status: 404, body: { error: 'Not Found' }.to_json)
        end

        it 'raises an error' do
          expect { usernames }.to raise_error(StandardError, /Failed to fetch groups data/)
        end
      end
    end
  end

  describe '#find_labels_by_categories' do
    subject(:labels) { helper.find_labels_by_categories(*categories) }

    context 'when searching by a single category' do
      let(:categories) { ['api'] }

      it 'returns the matching group label' do
        expect(labels).to eq(['group::import and integrate'])
      end
    end

    context 'when searching by multiple categories from the same group' do
      let(:categories) { %w[api integrations] }

      it 'returns the label only once' do
        expect(labels).to eq(['group::import and integrate'])
      end
    end

    context 'when searching by categories from different groups' do
      let(:categories) { %w[api settings] }

      it 'returns labels from all matching groups' do
        expect(labels).to contain_exactly(
          'group::import and integrate',
          'group::personal productivity'
        )
      end
    end

    context 'when searching by non-existent category' do
      let(:categories) { ['unknown_category'] }

      it 'returns an empty array' do
        expect(labels).to be_empty
      end
    end

    context 'when searching without categories' do
      let(:categories) { [] }

      it 'returns an empty array' do
        expect(labels).to be_empty
      end
    end
  end
end
