# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Notifier do
  let(:env) do
    { 'TOP_UPSTREAM_SOURCE_PROJECT' => 'example', 'CI_PROJECT_ID' => '1', 'CI_SELF_TRIGGERED' => ci_self_triggered }
  end

  shared_examples_for "A result" do |label|
    let(:arguments) { ['spec/fixtures/batched-background-migrations/'] }
    let(:project_path) { 'example' }

    let(:merge_request_id) { 137264 }

    let(:gitlab_client) do
      instance_double('client', create_merge_request_note: 'example', merge_request: merge_request)
    end

    let(:merge_request) do
      instance_double(
        'merge_request',
        labels: ['fake_label'],
        author: instance_double('author', username: 'fake_username')
      )
    end

    before do
      allow(Gitlab).to receive(:client).and_return(gitlab_client)

      stub_const('ENV', { 'TOP_UPSTREAM_SOURCE_PROJECT' => project_path })
    end

    it_behaves_like 'creating and updating a note with the proper id'

    context 'when an exeception is raised' do
      before do
        allow(Gitlab).to receive(:client).and_raise(Net::OpenTimeout)
      end

      it 'does not create a merge request note' do
        expect(gitlab_client).not_to receive(:create_merge_request_note)
      end
    end

    it 'creates a merge request note and updates the labels' do
      expect(gitlab_client).to receive(:create_merge_request_note).and_return('done')

      expect(gitlab_client).to receive(:update_merge_request).with(
        project_path, merge_request_id, add_labels: [label]
      ).and_return('done')

      subject
    end

    context 'when the label is already present' do
      let(:merge_request) do
        instance_double(
          'merge_request',
          labels: [label],
          author: instance_double('author', username: 'fake_username')
        )
      end

      it 'does not create the merge request' do
        expect(gitlab_client).not_to receive(:create_merge_request_note)

        subject
      end
    end
  end

  shared_examples 'creating a note with the proper id' do
    before do
      allow(Gitlab).to receive(:client).and_return(gitlab_client)

      stub_const('ENV', env)
    end

    context 'when triggering a self CI' do
      let(:ci_self_triggered) { 'true' }

      it 'uses self id to create a note' do
        expect(gitlab_client).to receive(:create_merge_request_note).with('1', *any_args).and_return('done')

        subject
      end
    end

    context 'when CI was triggered upstream' do
      let(:ci_self_triggered) { nil }

      it 'uses project path to create a note' do
        expect(gitlab_client).to receive(:create_merge_request_note).with('example', *any_args).and_return('done')

        subject
      end
    end
  end

  shared_examples 'creating and updating a note with the proper id' do
    before do
      allow(Gitlab).to receive(:client).and_return(gitlab_client)

      stub_const('ENV', env)
    end

    context 'when triggering a self CI' do
      let(:ci_self_triggered) { 'true' }

      it 'uses self id to create and update a note' do
        expect(gitlab_client).to receive(:create_merge_request_note).with('1', *any_args).and_return('done')
        expect(gitlab_client).to receive(:update_merge_request).with('1', *any_args).and_return('done')

        subject
      end
    end

    context 'when CI was triggered upstream' do
      let(:ci_self_triggered) { nil }

      it 'uses project path to create and update a note' do
        expect(gitlab_client).to receive(:create_merge_request_note).with('example', *any_args).and_return('done')
        expect(gitlab_client).to receive(:update_merge_request).with('example', *any_args).and_return('done')

        subject
      end
    end
  end

  describe '#unfinalized_batched_background_migration_checker' do
    subject { described_class.new.invoke(:unfinalized_batched_background_migration_checker, arguments) }

    before do
      allow(Gitlab).to receive(:client).and_return(gitlab_client)

      stub_const('ENV', { 'TOP_UPSTREAM_SOURCE_PROJECT' => 'example' })
    end

    let(:gitlab_client) { instance_double('client', create_merge_request_note: 'example') }

    context 'when `finalized_by` is null' do
      let(:arguments) { ['spec/fixtures/batched-background-migrations/finalized'] }

      it 'does not create a merge request note' do
        expect(gitlab_client).not_to receive(:create_merge_request_note)

        subject
      end
    end

    context 'when `finalize_after` is lesser than `Date.today`' do
      let(:arguments) { ['spec/fixtures/batched-background-migrations/'] }

      before do
        allow(Date).to receive(:today).and_return Date.new(2023, 1, 1)
      end

      it 'creates a merge request note' do
        expect(gitlab_client).to receive(:create_merge_request_note).and_return('done')

        subject
      end

      it_behaves_like 'creating a note with the proper id'
    end
  end

  describe '#failed_batched_background_migration_checker' do
    subject { described_class.new.invoke(:failed_batched_background_migration_checker, arguments) }

    it_behaves_like 'A result', 'background-migration::failed'
  end

  describe '#active_batched_background_migration_checker' do
    subject { described_class.new.invoke(:active_batched_background_migration_checker, arguments) }

    it_behaves_like 'A result', 'background-migration::active'
  end

  describe '#finished_batched_background_migration_checker' do
    subject { described_class.new.invoke(:finished_batched_background_migration_checker, arguments) }

    it_behaves_like 'A result', 'background-migration::finished'
  end

  describe '#finalized_batched_background_migration_checker' do
    subject { described_class.new.invoke(:finalized_batched_background_migration_checker, arguments) }

    it_behaves_like 'A result', 'background-migration::finalized'
  end
end
