# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Migration do
  let(:good_queries) do
    [
      {
        "query" => "select pg_database_size(current_database()) /*application:test*/",
        "expected_query" => "select pg_database_size(current_database())",
        "calls" => 1,
        "total_time" => 87.621755,
        "max_time" => 87.621755,
        "mean_time" => 87.621755,
        "rows" => 1
      },
      {
        "query" => "select * from users limit 1 /*application:test*/",
        "expected_query" => "select * from users limit 1",
        "calls" => 1,
        "total_time" => 80.51234,
        "max_time" => 80.51234,
        "mean_time" => 80.51234,
        "rows" => 1
      },
      {
        "query" => "CREATE TABLE \"users\" (\"id\" bigserial primary key, \"created_at\" timestamp(6) NOT NULL) "\
                   "/*application:test*/",
        "expected_query" => 'CREATE TABLE "users" ("id" bigserial primary key, "created_at" timestamp(6) NOT NULL)',
        "calls" => 1,
        "total_time" => 80.51234,
        "max_time" => 80.51234,
        "mean_time" => 80.51234,
        "rows" => 0
      }
    ]
  end

  let(:bad_query) do
    {
      "query" => "select * from users limit 1000 /*application:test*/",
      "expected_query" => "select * from users limit 1000",
      "calls" => 1,
      "total_time" => 222.49203,
      "max_time" => Query::QUERY_GUIDANCE_MILLISECONDS * 2,
      "mean_time" => 222.49203,
      "rows" => 1
    }
  end

  let(:concurrent_query) do
    {
      "query" => "create index concurrently tmp_index ON tmp_table (id)",
      "expected_query" => "create index concurrently tmp_index ON tmp_table (id)",
      "calls" => 1,
      "total_time" => 87.621755,
      "max_time" => 87.621755,
      "mean_time" => 87.621755,
      "rows" => 1
    }
  end

  let(:queries) { good_queries + [bad_query] }

  let(:stats) do
    {
      "migration" => 20210602144718,
      "walltime" => 1.9509822260588408,
      "success" => true,
      "total_database_size_change" => 32768,
      "query_statistics" => queries
    }
  end

  let(:bbm_error_stats) do
    {
      "migration" => 20210602144718,
      "walltime" => 1.9509822260588408,
      "success" => false,
      "total_database_size_change" => 32768,
      "query_statistics" => queries,
      "error_message" => "Cannot finalize a batched background migration that is still running"
    }
  end

  let(:migration_hash) do
    {
      "version" => 20210602144718,
      "path" => "db/migrate/20210602144718_create_test_table.rb",
      "name" => "CreateTestTable",
      "type" => "regular",
      "intro_on_current_branch" => true
    }
  end

  subject(:migration) { described_class.new(migration_hash, stats, []) }

  describe '.from_directory' do
    let(:migration) { described_class.from_directory(directory, global_migration_data: global_migration_data) }

    context 'when building the migration object based on V3 artifacts' do
      let(:directory) { Pathname.new('spec/fixtures/migration-testing/v3/up/AddMigrationThatRequiresBackground') }
      let(:global_migration_data) { JSON.parse(File.read('spec/fixtures/migration-testing/v3/migrations.json')) }

      it 'builds the migration object correctly' do
        expect(migration.version).to eq(20201015154529)
      end
    end

    context 'when building the migration object based on V4 artifacts' do
      let(:directory) do
        Pathname.new('spec/fixtures/migration-testing/v4/main/up/CreateEfficientlyOrderedColumnsTable')
      end

      let(:global_migration_data) { JSON.parse(File.read('spec/fixtures/migration-testing/v4/migrations.json')) }

      it 'builds the migration object correctly' do
        expect(migration.version).to eq(20221130142531)
      end
    end
  end

  it 'loads from the result json hash' do
    expect { subject }.not_to raise_error
  end

  it 'collects queries into query objects' do
    expect(subject.queries.size).to eq(4)
    expect(subject.queries.first).to be_a(Query)
  end

  context 'when no query statistics' do
    let(:queries) { nil }

    it 'does not throw error' do
      expect { subject.queries }.not_to raise_error
    end
  end

  describe 'types predicates' do
    context 'with "regular" type' do
      it { is_expected.to be_regular }
      it { is_expected.not_to be_post_deploy }
      it { is_expected.not_to be_background_batch }
    end

    context 'with "post_deploy" type' do
      before do
        migration_hash['type'] = described_class::TYPE_POST_DEPLOY
      end

      it { is_expected.not_to be_regular }
      it { is_expected.to be_post_deploy }
      it { is_expected.not_to be_background_batch }
    end

    context 'with "background_batch" type' do
      before do
        migration_hash['type'] = described_class::TYPE_BACKGROUND_BATCH
      end

      it { is_expected.not_to be_regular }
      it { is_expected.not_to be_post_deploy }
      it { is_expected.to be_background_batch }
    end
  end

  describe '#was_run?' do
    it 'returns false if the migration has no statistics' do
      subject = described_class.new(migration_hash, nil, [])

      expect(subject.was_run?).to be false
    end

    it 'returns true if the migration has statistics' do
      expect(subject.was_run?).to be true
    end
  end

  describe '#concurrent_only?' do
    context 'when all important commands are concurrent' do
      let(:queries) { [concurrent_query] }

      it { is_expected.to be_concurrent_only }
    end

    context 'when at least one important commands is not concurrent' do
      let(:queries) { [concurrent_query, bad_query] }

      it { is_expected.not_to be_concurrent_only }
    end
  end

  describe '#time_guidance' do
    context 'with regular migration' do
      it 'is 3 minutes' do
        expect(subject.time_guidance).to eq 3.minutes
      end
    end

    context 'with post_deploy migration' do
      before do
        migration_hash['type'] = 'post_deploy'
      end

      it 'is 10 minutes' do
        expect(subject.time_guidance).to eq 10.minutes
      end

      context 'with concurrent commands only' do
        let(:queries) { [concurrent_query] }

        it 'is 20 minutes' do
          expect(subject.time_guidance).to eq 20.minutes
        end
      end
    end
  end

  describe '#exceeds_time_guidance?' do
    shared_examples 'has time_guidance limit' do |limit_in_seconds|
      it "is false if <= #{limit_in_seconds}" do
        stats['walltime'] = limit_in_seconds - 1

        expect(subject.exceeds_time_guidance?).to be false
      end

      it "is true if > #{limit_in_seconds}" do
        stats['walltime'] = limit_in_seconds + 1

        expect(subject.exceeds_time_guidance?).to be true
      end
    end

    context 'with a regular migration' do
      include_examples 'has time_guidance limit', described_class::REGULAR_MIGRATION_GUIDANCE_SECONDS
    end

    context 'with a post_deploy migration' do
      before do
        migration_hash['type'] = described_class::TYPE_POST_DEPLOY
      end

      include_examples 'has time_guidance limit', described_class::POST_DEPLOY_MIGRATION_GUIDANCE_SECONDS

      context 'with concurrent commands only' do
        let(:queries) { [concurrent_query] }

        include_examples 'has time_guidance limit', described_class::POST_DEPLOY_CONCURRENT_MIGRATION_GUIDANCE_SECONDS
      end
    end
  end

  describe '#queries_with_warnings' do
    it 'finds queries that exceed timing guidelines' do
      expect(subject.queries_with_warnings.size).to eq(1)
    end
  end

  describe '#warning?' do
    let(:queries) { good_queries }

    it 'is false if everything works' do
      expect(subject.warning?).to be false
    end

    it 'is true if not successful' do
      subject.success = false

      expect(subject.warning?).to be true
    end

    it 'is true if migration took too long' do
      subject.walltime = described_class::POST_DEPLOY_MIGRATION_GUIDANCE_SECONDS * 5

      expect(subject.warning?).to be true
    end

    it 'is true if a query took too long' do
      queries << bad_query

      expect(subject.warning?).to be true
    end
  end

  describe '#warnings' do
    let(:queries) { good_queries }

    context 'when everything is okay' do
      it 'has no warnings' do
        expect(subject.warnings.length).to eq(0)
      end
    end

    context 'when we tried to finish a still-running batched background migration' do
      let(:stats) { bbm_error_stats }

      it 'has two warnings if there is an error message concerning the migration' do
        expect(subject.warnings.length).to eq(2)
      end
    end
  end

  describe '#important_queries' do
    it 'returns only unexcluded queries' do
      expect(subject.important_queries.size).to eq(3)
    end
  end

  describe '#create_table_queries' do
    it 'returns only create table queries' do
      create_queries = queries.select { |q| q["query"].downcase.include?(Query::CREATE_TABLE_STATMENT) }

      expect(subject.create_table_queries.count).to eq(1)
      expect(subject.create_table_queries.map(&:query)).to eq(create_queries.pluck("expected_query"))
    end
  end

  describe '#sort_key' do
    it 'returns a representation of type and version' do
      post_migration = described_class.new(
        { 'version' => 42, 'type' => Migration::TYPE_POST_DEPLOY, 'intro_on_current_branch' => true },
        nil,
        []
      )

      expect(subject.sort_key).to eq([0, 20210602144718])
      expect(post_migration.sort_key).to eq([1, 42])
    end
  end
end
