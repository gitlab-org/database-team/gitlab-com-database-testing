# frozen_string_literal: true

require 'spec_helper'

RSpec.describe FinishedBatchedBackgroundMigration do
  let(:migration) { { "migration_job_name" => "BackfillVsCodeSettingsUuid" } }

  subject(:message) { described_class.message(migration) }

  describe '.message' do
    let(:result) do
      <<~MARKDOWN
        The BackfillVsCodeSettingsUuid batched background migration is completed!

        _This is an auto-generated comment from [gitlab-com-database-testing/-/merge_requests/190](https://gitlab.com/gitlab-org/database-team/gitlab-com-database-testing/-/merge_requests/190)._
      MARKDOWN
    end

    it 'returns message' do
      expect(message).to eql(result)
    end
  end
end
