# frozen_string_literal: true
# require 'failure_notice'

RSpec.describe FailureNotice do
  let(:expected_failure_message) { File.read(file_fixture('migration-testing/v4/expected-failure-notice.txt')) }

  describe '::message' do
    it 'returns a message template as a string' do
      ENV['CI_PIPELINE_ID'] = '12345'
      ENV['CI_PIPELINE_URL'] = 'https://test.gitlab.com/12345'
      failure_notice = described_class.message
      expect(failure_notice.force_encoding('ASCII-8BIT')).to eq(expected_failure_message.force_encoding('ASCII-8BIT'))
    end
  end
end
