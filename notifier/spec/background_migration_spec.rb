# frozen_string_literal: true
require 'spec_helper'

RSpec.describe BackgroundMigration do
  subject(:background_migration) do
    described_class.new(name: 'bg', interval: interval, total_tuple_count: total_tuple_count,
                        max_batch_size: max_batch_size, execution_details: execution_details,
                        batches: batches)
  end

  let(:interval) { nil }
  let(:total_tuple_count) { nil }
  let(:max_batch_size) { nil }
  let(:execution_details) { nil }

  describe '#queries' do
    let(:aggregate_query_1) { background_migration.queries.find { |q| q.fingerprint == '50fde20626009aba' } }
    let(:aggregate_query_2) { background_migration.queries.find { |q| q.fingerprint == 'b2b24be3139d111f' } }

    let(:queries_migration_1) do # query, calls, total_time, max_time, mean_time, rows
      [
        Query.new(
          { query: 'SELECT 1', total_time: 100, max_time: 100, mean_time: 100, rows: 1, calls: 1 }.stringify_keys),
        Query.new(
          {
            query: 'SELECT 1, 1 FROM foo',
            total_time: 200,
            max_time: 200,
            mean_time: 200,
            rows: 2,
            calls: 2
          }.stringify_keys)
      ]
    end

    let(:queries_migration_2) do
      [
        Query.new({ query: 'SELECT 1', total_time: 50, max_time: 50, mean_time: 50, rows: 3, calls: 3 }.stringify_keys),
        Query.new({ query: 'SELECT 1', total_time: 25, max_time: 25, mean_time: 25, rows: 4, calls: 4 }.stringify_keys)
      ]
    end

    let(:batches) do
      [
        instance_double(Migration, queries: queries_migration_1),
        instance_double(Migration, queries: queries_migration_2)
      ]
    end

    it 'has the correct number of aggregated queries' do
      expect(background_migration.queries.count).to eq(2)
    end

    it 'sums call counts per query text' do
      expect(aggregate_query_1.calls).to eq(8)
      expect(aggregate_query_2.calls).to eq(2)
    end

    it 'sums row counts per query text' do
      expect(aggregate_query_1.rows).to eq(8)
      expect(aggregate_query_2.rows).to eq(2)
    end

    it 'sums total_time per query text' do
      expect(aggregate_query_1.total_time).to eq(175)
      expect(aggregate_query_2.total_time).to eq(200)
    end

    it 'weighted averages mean query time by number of calls' do
      expect(aggregate_query_1.mean_time).to eq((100 * 1 + 50 * 3 + 25 * 4) / (1 + 3 + 4))
      expect(aggregate_query_2.mean_time).to eq(200)
    end

    it 'maxes max query time per query text' do
      expect(aggregate_query_1.max_time).to eq(100)
      expect(aggregate_query_2.max_time).to eq(200)
    end

    context 'when queries in a given migration only differ by number of params in an IN clause' do
      let(:bbm_batch) do
        query_texts = [
          'SELECT foo FROM bar WHERE id IN (1, 2, 3, 4, 5)',
          'SELECT foo FROM bar WHERE id IN (1, 2, 3, 4)',
          'SELECT foo FROM bar WHERE id IN (1)'
        ]

        instance_double(Migration, queries: query_texts.map do |qt|
          Query.new({
            query: qt,
            total_time: 100,
            max_time: 20,
            mean_time: 10,
            rows: 5,
            calls: 5
          }.stringify_keys)
        end)
      end

      let(:batches) { [bbm_batch] }

      it 'groups them by fingerprint and collapses the IN clause' do
        expect(background_migration.queries.count).to eq(1)
        expect(background_migration.queries.first.query).to eq(
          'SELECT foo FROM bar WHERE id IN ($1)'
        )
      end

      it 'properly aggregates the statistics from the collapsed queries' do
        expect(background_migration.queries.first.max_time).to eq(20)
        expect(background_migration.queries.first.total_time).to eq(300)
        expect(background_migration.queries.first.mean_time).to eq(10)
        expect(background_migration.queries.first.rows).to eq(15)
        expect(background_migration.queries.first.calls).to eq(15)
      end

      it 'sets the "collapsed" property on the collapsed query' do
        expect(background_migration.queries.first.collapsed).to be(true)
      end
    end
  end

  describe '#time_to_complete' do
    let(:interval) { 120 }
    let(:total_tuple_count) { 772_321 }
    let(:batch_size) { 5_000 }
    let(:max_batch_size) { 5_000 }
    let(:batches) { [] }
    let(:execution_details) do
      [
        { 'time_spent' => 49.61, 'batch_size' => batch_size },
        { 'time_spent' => 4.25, 'batch_size' => batch_size },
        { 'time_spent' => 55.92, 'batch_size' => batch_size },
        { 'time_spent' => 68.49, 'batch_size' => batch_size }
      ]
    end

    it { expect(background_migration.time_to_complete).to eq('5 hours and 8 minutes') }

    context 'when interval is increased up to 4 minutes' do
      let(:interval) { 4.minutes }

      it 'doubles the estimation time' do
        expect(background_migration.time_to_complete).to eq('10 hours and 16 minutes')
      end
    end

    context 'when batch_size is increased to 10,000' do
      let(:batch_size) { 10_000 }
      let(:max_batch_size) { 10_000 }

      it 'reduces estimation time by half' do
        expect(background_migration.time_to_complete).to eq('2 hours and 34 minutes')
      end
    end

    context 'when total_tuple_count increases' do
      let(:total_tuple_count) { 1_000_000 }

      it { expect(background_migration.time_to_complete).to eq('6 hours and 40 minutes') }
    end

    context 'when sampling artifacts are not present' do
      let(:interval) { nil }
      let(:total_tuple_count) { nil }
      let(:max_batch_size) { nil }
      let(:batches) { [] }
      let(:execution_details) { nil }

      it { expect(background_migration.time_to_complete).to eq('0 seconds') }
    end

    context 'when total_tuple_count and max_batch_size are nil' do
      let(:total_tuple_count) { nil }
      let(:max_batch_size) { nil }
      let(:interval) { 120 }
      let(:batch_size) { 5_000 }
      let(:batches) { [] }

      it { expect(background_migration.time_to_complete).to eq('0 seconds') }
    end
  end
end
