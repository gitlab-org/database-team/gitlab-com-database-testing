# frozen_string_literal: true

require 'spec_helper'

RSpec.describe MigrationFailureNotice do
  describe 'end to end test for rendering a migration failure notice' do
    let(:fixture_root) { 'v4' }

    subject { described_class.new(result) }

    context 'when there is a failure' do
      let(:result) { MultiDbResult.from_directory(file_fixture("migration-testing/#{fixture_root}")) }

      let(:expected_migration_failure_notice) do
        file_fixture("migration-testing/#{fixture_root}/expected-migration-failure-notice.txt")
      end

      before do
        override_env_from_fixture('migration-testing/environment.json')
      end
      # The expectation for this spec lives in `expected_comment_file`
      # It can be re-recorded with: `RECAPTURE_END_TO_END_RESULTS=1 bundle exec rspec spec`

      describe '#render' do
        it 'renders the comment for fixtures' do
          if ENV['RECAPTURE_END_TO_END_RESULTS']
            File.open(expected_migration_failure_notice, 'wb+') do |io|
              io << subject
            end
          end

          expect(subject.render).to eq(File.read(expected_migration_failure_notice))
        end
      end

      describe '#has_failed_migrations?' do
        it 'returns true' do
          expect(subject).to have_failed_migrations
        end
      end
    end

    context 'when there are no failures' do
      let(:main_db_result) { instance_double('Result', migrations: [], background_migrations: []) }
      let(:ci_db_result) { instance_double('Result', migrations: [], background_migrations: []) }

      let(:result) do
        instance_double('MultiDbResult', per_db_results: { 'main' => main_db_result, 'ci' => ci_db_result })
      end

      describe '#render' do
        it 'renders the comment for fixtures' do
          expect(subject.render).to be_nil
        end
      end

      describe '#has_failed_migrations?' do
        it 'returns false' do
          expect(subject).not_to have_failed_migrations
        end
      end
    end
  end
end
