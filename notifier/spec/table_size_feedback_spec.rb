# frozen_string_literal: true

require 'spec_helper'

RSpec.describe TableSizeFeedback do
  describe "#render_table_size_summary" do
    before do
      allow(Date).to receive(:current).and_return('2025-02-27')
    end

    subject { described_class.new(data).render_table_size_summary }

    let(:path) { 'spec/fixtures/table-sizes-output.json' }
    let(:data) { JSON.parse(File.read(path)).first }

    let(:markdown_table) do
      <<~TEXT
        ## Summary

        > [!warning]

        Table `merge_request_diff_commits` is rapidly growing toward our limits. It requires immediate attention to prevent performance degradation.

        ## Details

        | Date | Identifier | Total size | Table size | Index size | Classification |
        | -------- | -------- | -------- | -------- | -------- | -------- |
        | `2025-02-27` | `public.merge_request_diff_commits` | `6269 GB` | `5105 GB` | `1165 GB` | `over_limit` |

        ## Growth Concerns

        - The current classification is **over_limit**, but the rapid growth rate indicates action is required.

        ## Recommended Actions

        1. **Retention**: Delete unnecessary data, for example expire old and unneeded records.
        1. **Index optimization**: Drop unnecessary indexes and consolidate overlapping indexes if possible.
        1. **Optimize data types**: Review data type decisions and optimize data types where possible (example: use integer instead of text for an enum column)
        1. **Partitioning**: Apply a partitioning scheme if there is a common access dimension.
        1. **Normalization**: Review relational modeling and apply normalization techniques to remove duplicate data
        1. **Vertical table splits**: Review column usage and split table vertically.
        1. **Externalize**: Move large data types out of the database entirely. For example, JSON documents, especially when not used for filtering, may be better stored outside the database, for example, in object storage.

        Reach out to [#database-sustainability](https://gitlab.enterprise.slack.com/archives/C08GQHWGM0S) for assistance.

        ## Impact

        Without action: degraded code review performance, increased maintenance overhead, and potential service disruptions.

        ## Documentation

        - [Large tables limitations](https://docs.gitlab.com/ee/development/database/large_tables_limitations.html)
        - [Retention policy guidelines](https://docs.gitlab.com/development/data_retention_policies.html)
        - [Blueprint](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/database_size_limits)

        <!-- 588448f9b33662cbd6eb6db703358e0e351319535b098469e14a7add838be33f -->
        <!-- STATUS NOTE START --><!-- STATUS NOTE END -->
      TEXT
    end

    it "returns a markdown table" do
      expect(subject).to eql markdown_table
    end
  end
end
