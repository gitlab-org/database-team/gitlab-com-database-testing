# frozen_string_literal: true

require 'spec_helper'

RSpec.describe FailedBatchedBackgroundMigration do
  let(:migration) { { "migration_job_name" => "BackfillVsCodeSettingsUuid" } }
  let(:author_username) { 'diogo' }

  subject(:message) { described_class.message(migration, author_username) }

  describe '.message' do
    let(:result) do
      <<~MARKDOWN
        The status of the #{migration['migration_job_name']} batched background migration is failed.

        _This is an auto-generated comment from [gitlab-com-database-testing/-/merge_requests/177](https://gitlab.com/gitlab-org/database-team/gitlab-com-database-testing/-/merge_requests/177)._

        cc: @#{author_username} @gitlab-org/database-team/triage
      MARKDOWN
    end

    it 'returns message' do
      expect(message).to eql(result)
    end
  end
end
