# frozen_string_literal: true
require 'spec_helper'

RSpec.describe Query do
  let(:pgss) do
    {
      "query" => "select pg_database_size(current_database()) /*application:test*/",
      "calls" => 1,
      "total_time" => 269.888734,
      "max_time" => 269.888734,
      "mean_time" => 269.888734,
      "rows" => 0
    }
  end

  subject(:query) { described_class.new(pgss) }

  it 'inits from a pgpass row' do
    expect { subject }.not_to raise_error
  end

  describe 'excluded?' do
    it 'delegates to QueryExclusion' do
      result = double
      expect(QueryExclusion).to receive(:exclude?).with(subject.query).and_return(result)
      expect(subject.excluded?).to eq(result)
    end
  end

  describe '#new_table_fields_and_types' do
    let(:pgss) { { "query" => query } }

    let(:query) do
      <<~SQL.squish
        CREATE TABLE accounts (
          user_id serial PRIMARY KEY,
          username VARCHAR ( 50 ) UNIQUE NOT NULL,
          password VARCHAR ( 50 ) NOT NULL,
          email VARCHAR ( 255 ) UNIQUE NOT NULL,
          created_on TIMESTAMP NOT NULL,
          last_login TIMESTAMP
        );
      SQL
    end

    let(:result) do
      [
        %w[user_id serial],
        %w[username varchar],
        %w[password varchar],
        %w[email varchar],
        %w[created_on timestamp],
        %w[last_login timestamp]
      ]
    end

    it 'returns column names and data types' do
      expect(subject.new_table_fields_and_types).to eql(result)
    end

    context 'when is not a create table statement' do
      let(:query) { 'select pg_database_size(current_database()) /*application:test*/' }

      it 'returns nil' do
        expect(subject.new_table_fields_and_types).to be_nil
      end
    end
  end

  describe '#formatted_query' do
    let(:normalized_query) { 'Select $1 from users where email=$2 limit $3' }

    context 'when the query is not normalized' do
      let(:pgss) do
        {
          "query" => "Select 'somevalue' from users where email='user@gitlab.com' limit 1",
          "calls" => 1,
          "total_time" => 1824825.496259,
          "max_time" => 1824825.496259,
          "mean_time" => 1824825.496259,
          "rows" => 0
        }
      end

      it 'returns a normalized query' do
        expect(subject.formatted_query).to eql(normalized_query)
      end
    end

    context 'when the query is already normalized' do
      let(:pgss) do
        {
          "query" => "Select $1 from users where email=$2 limit $3",
          "calls" => 1,
          "total_time" => 1824825.496259,
          "max_time" => 1824825.496259,
          "mean_time" => 1824825.496259,
          "rows" => 0
        }
      end

      it 'returns a normalized query' do
        expect(subject.formatted_query).to eql(normalized_query)
      end
    end

    context 'when query is of type "CREATE TRIGGER ... EXECUTE FUNCTION ..."' do
      let(:pgss) do
        # rubocop:disable Layout/LineLength
        {
          "query" => "CREATE TRIGGER trigger_has_external_wiki_on_delete_new AFTER DELETE ON integrations FOR EACH ROW WHEN (((old.type_new)::text = 'Integrations::ExternalWiki'::text) AND (old.project_id IS NOT NULL)) EXECUTE FUNCTION set_has_external_wiki(); /*application:test*/",
          "calls" => 1,
          "total_time" => 1824825.496259,
          "max_time" => 1824825.496259,
          "mean_time" => 1824825.496259,
          "rows" => 0
        }
        # rubocop:enable Layout/LineLength
      end

      it 'parse it successfully' do
        expect { query.formatted_query }.not_to raise_error
      end
    end

    context 'when query have comments' do
      where(:raw_query, :expected_query) do
        [
          [
            "/*BEGINNING COMMENT*/ SELECT /*MIDDLE COMMENT*/ * FROM \"users\" WHERE \"users\".\"email\" =" \
          " 'l.rosa@gitlab.com' /*application:test*/",
            'SELECT *<br />  FROM "users"  WHERE "users"."email" = $1'
          ],
          [
            "/*BEGINNING COMMENT*/ CREATE INDEX CONCURRENTLY \"tmp_index_merge_requests_draft_and_status\"" \
          " /*MIDDLE COMMENT*/ ON \"merge_requests\" (\"id\"); /*application:test*/",
            'CREATE INDEX CONCURRENTLY "tmp_index_merge_requests_draft_and_status" ON "merge_requests" ("id");'
          ],
          [
            "/*BEGINNING COMMENT*/ CREATE TRIGGER trigger_has_external_wiki_on_delete_new AFTER DELETE ON" \
          " /*MIDDLE COMMENT*/ integrations FOR EACH ROW WHEN (((old.type_new)::text =" \
          " 'Integrations::ExternalWiki'::text) AND (old.project_id IS NOT NULL)) EXECUTE FUNCTION" \
          " set_has_external_wiki(); /*application:test*/",
            "CREATE TRIGGER trigger_has_external_wiki_on_delete_new AFTER DELETE ON integrations FOR EACH ROW WHEN" \
          " (((old.type_new)::text = 'Integrations::ExternalWiki'::text) AND (old.project_id IS NOT NULL))" \
          " EXECUTE FUNCTION set_has_external_wiki();"
          ],
          [
            "/*BEGINNING COMMENT*/ CREATE TABLE accounts(id bigserial PRIMARY KEY); /*application:test*/",
            "CREATE TABLE accounts(id bigserial PRIMARY KEY);"
          ],
          [
            'CREATE INDEX CONCURRENTLY "tmp_index_merge_requests_draft_and_status" ON "merge_requests" ("id");',
            'CREATE INDEX CONCURRENTLY "tmp_index_merge_requests_draft_and_status" ON "merge_requests" ("id");'
          ]
        ]
      end

      with_them do
        let(:pgss) do
          {
            "query" => raw_query,
            "calls" => 1,
            "total_time" => 1824825.496259,
            "max_time" => 1824825.496259,
            "mean_time" => 1824825.496259,
            "rows" => 0
          }
        end

        it 'removes all query comments' do
          expect(subject.formatted_query).to eq(expected_query)
        end
      end
    end
  end

  describe '#time_guidance' do
    context 'with regular query' do
      it 'is 100ms' do
        expect(subject.time_guidance).to eq 100
      end
    end

    context 'with concurrent query' do
      subject(:query) { described_class.new(pgss, execution_context: migration) }

      before do
        pgss['query'] = 'CREATE INDEX CONCURRENTLY index_ci_runners_on_token_lower ' \
                        'ON ci_runners (LOWER(token)) /*application:test*/'
      end

      context 'with regular migration' do
        let(:migration) { Migration.new({ 'type' => Migration::TYPE_REGULAR }, nil, []) }

        it 'is 5 minutes' do
          expect(subject.time_guidance).to eq 5.minutes.in_milliseconds
        end
      end

      context 'with post_deploy migration' do
        let(:migration) { Migration.new({ 'type' => Migration::TYPE_POST_DEPLOY }, nil, []) }

        it 'is 20 minutes' do
          expect(subject.time_guidance).to eq 20.minutes.in_milliseconds
        end
      end
    end
  end

  describe '#exceeds_time_guidance?' do
    shared_examples 'has time_guidance limit' do |limit_in_milliseconds|
      it "is true if max time > #{limit_in_milliseconds}" do
        subject.max_time = limit_in_milliseconds + 1

        expect(subject.exceeds_time_guidance?).to be true
      end

      it "is false if max time <= #{limit_in_milliseconds}" do
        subject.max_time = limit_in_milliseconds - 1

        expect(subject.exceeds_time_guidance?).to be false
      end
    end

    context 'with regular query' do
      include_examples 'has time_guidance limit', described_class::QUERY_GUIDANCE_MILLISECONDS
    end

    context 'with concurrent query' do
      subject(:query) { described_class.new(pgss, execution_context: migration) }

      before do
        pgss['query'] = 'CREATE INDEX CONCURRENTLY index_ci_runners_on_token_lower ' \
                        'ON ci_runners (LOWER(token)) /*application:test*/'
      end

      context 'with regular migration' do
        let(:migration) { Migration.new({ 'type' => Migration::TYPE_REGULAR }, nil, []) }

        include_examples 'has time_guidance limit', described_class::CONCURRENT_QUERY_GUIDANCE_MILLISECONDS
      end

      context 'with post_deploy migration' do
        let(:migration) { Migration.new({ 'type' => Migration::TYPE_POST_DEPLOY }, nil, []) }

        include_examples 'has time_guidance limit', described_class::CONCURRENT_POSTMIGRATE_QUERY_GUIDANCE_MILLISECONDS
      end
    end
  end

  describe '#unoptimized_create_queries_data' do
    context 'with non create table queries' do
      it 'returns empty hash' do
        expect(query.create_table?).to be false
        expect(described_class.unoptimized_create_queries_data([query]).present?).to be false
      end
    end

    context 'with efficiently ordered create table queries' do
      let(:pgss) { { "query" => query } }
      let(:query) do
        <<~SQL.squish
          CREATE TABLE accounts (
            id bigserial PRIMARY KEY,
            created_on timestamp NOT NULL,
            sync_on timestamptz NOT NULL,
            dob date,
            sync_state smallint
          );
        SQL
      end

      it 'returns empty hash' do
        expect(subject.create_table?).to be true
        expect(described_class.unoptimized_create_queries_data([subject]).present?).to be false
      end
    end

    context 'with inefficiently ordered create table queries' do
      let(:pgss) { { "query" => query } }
      let(:query) do
        <<~SQL.squish
          CREATE TABLE accounts (
            id bigserial PRIMARY KEY,
            dob date,
            created_on timestamp NOT NULL
          );
        SQL
      end

      it 'returns queries with new table data' do
        expect(subject.create_table?).to be true
        expect(described_class.unoptimized_create_queries_data([subject]).present?).to be true
      end
    end
  end

  context 'when query has special characters' do
    let(:pgss) do
      {
        "query" => "CREATE INDEX CONCURRENTLY \"tmp_index_merge_requests_draft_and_status\"" \
                   " ON \"merge_requests\" (\"id\") WHERE draft = false AND state_id = 1 AND " \
                   "((title)::text ~* '^\\[draft\\]|\\(draft\\)|draft:|draft|\\[WIP\\]|WIP:|WIP'::text) " \
                   "/*application:test*/",
        "calls" => 1,
        "total_time" => 1824825.496259,
        "max_time" => 1824825.496259,
        "mean_time" => 1824825.496259,
        "rows" => 0
      }
    end

    it 'replaces the pipe character with &#124;' do
      expect(subject.formatted_query).not_to include('|')
    end
  end

  describe '#concurrent?' do
    it 'is false for select query' do
      pgss['query'] = 'SELECT * FROM my_table'

      expect(subject).not_to be_concurrent
    end

    it 'is false for create index query' do
      pgss['query'] = 'CREATE INDEX my_tmp ON my_table (id)'

      expect(subject).not_to be_concurrent
    end

    it 'is true for concurrent create index query' do
      pgss['query'] = 'CREATE INDEX CONCURRENTLY my_tmp ON my_table (id)'

      expect(subject).to be_concurrent
    end

    it 'is true for concurrent drop index query' do
      pgss['query'] = 'DROP INDEX CONCURRENTLY my_tmp'

      expect(subject).to be_concurrent
    end
  end
end
