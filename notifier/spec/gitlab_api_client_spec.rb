# frozen_string_literal: true

require 'spec_helper'

RSpec.describe GitlabApiClient do
  subject(:api_client) { described_class.new(endpoint).call }

  describe '#call' do
    let(:endpoint) { 'https://gitlab.com/api/v4' }
    let(:ci_variables) { {} }
    let(:project) { 'gitlab-org/gitlab' }
    let(:api_token) { 'default-project-token' }
    let(:env) do
      { 'TOP_UPSTREAM_SOURCE_PROJECT' => project, 'GITLAB_API_TOKEN' => api_token }.merge(ci_variables)
    end

    before do
      stub_const('ENV', env)
    end

    context 'when no PAT is defined for the upstream project' do
      it { expect(api_client.private_token).to eq('default-project-token') }
    end

    context 'when PAT is defined for upstream project' do
      let(:project) { 'gitlab-org/security/gitlab' }
      let(:ci_variables) do
        { 'GITLAB_ORG_SECURITY_GITLAB_API_TOKEN' => 'private-project-token' }
      end

      it { expect(api_client.private_token).to eq('private-project-token') }
    end
  end
end
