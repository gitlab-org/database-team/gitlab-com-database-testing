# frozen_string_literal: true
require 'spec_helper'

RSpec.describe BatchedBackgroundMigrationStatus do
  describe '#notify' do
    subject(:notify) do
      described_class.new(gitlab_client, project_path).notify(batched_migrations, label, notifier_class)
    end

    let(:batched_migrations) { [{ "merge_request_id" => merge_request_id }] }

    let(:merge_request_id) { 123 }

    let(:label) { 'example' }

    let(:notifier_class) { FailedBatchedBackgroundMigration }

    let(:merge_request) do
      instance_double(
        'merge_request',
        labels: ['fake_label'],
        author: instance_double('author', username: 'fake_username')
      )
    end

    let(:gitlab_client) do
      instance_double(
        'client',
        create_merge_request_note: 'ok',
        update_merge_request: 'ok',
        merge_request: merge_request
      )
    end

    let(:project_path) { 'example' }

    it 'creates a merge request note and updates the labels' do
      expect(gitlab_client).to receive(:create_merge_request_note).and_return('ok')

      expect(gitlab_client).to receive(:update_merge_request).with(
        project_path, merge_request_id, add_labels: [label]
      ).and_return('ok')

      subject
    end

    context 'when an exeception is raised' do
      before do
        allow(gitlab_client).to receive(:merge_request).and_raise(Net::OpenTimeout)
      end

      it 'does not create a merge request note' do
        expect(gitlab_client).not_to receive(:create_merge_request_note)

        subject
      end
    end

    context 'when the label is already present' do
      let(:merge_request) do
        instance_double(
          'merge_request',
          labels: [label],
          author: instance_double('author', username: 'fake_username')
        )
      end

      it 'does not create the merge request' do
        expect(gitlab_client).not_to receive(:create_merge_request_note)

        subject
      end
    end
  end
end
