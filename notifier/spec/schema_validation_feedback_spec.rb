# frozen_string_literal: true

require 'spec_helper'

RSpec.describe SchemaValidationFeedback do
  describe "#render_schema_validation_summary" do
    subject { described_class.new(path).render_schema_validation_summary }

    let(:path) { 'spec/fixtures/' }

    let(:markdown_table) do
      <<~TEXT
       | Type | Object type | Object name | Table name| Structure SQL statement | Database statement |
       | -------- | -------- | -------- | ---------------- | -------------------- | --------------------- |
       | Gitlab::Schema::Validation::Validators::ExtraTables | Table | personal_access_tokens_restored_from_backup | personal_access_tokens_restored_from_backup |  | CREATE TABLE personal_access_tokens_restored_from_backup (id integer, last_used_at timestamp with time zone) |
      TEXT
    end

    it "returns a markdown table" do
      expect(subject).to eql markdown_table
    end
  end
end
