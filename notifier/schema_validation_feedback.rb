# frozen_string_literal: true

require 'erb'
require 'json'

FILE_NAME = 'output.json'

class SchemaValidationFeedback
  def initialize(path)
    @file_path = File.join(path, FILE_NAME)
  end

  def render_schema_validation_summary
    b = binding
    b.local_variable_set(:inconsistencies, inconsistencies)
    erb('schema_validation_table').result(b)
  end

  private

  def erb(template)
    ERB.new(File.read("templates/#{template}.erb"), trim_mode: '<>%')
  end

  def inconsistencies
    JSON.parse(File.read(file_path))
  end

  attr_reader :file_path
end
