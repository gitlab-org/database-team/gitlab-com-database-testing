# frozen_string_literal: true

class BatchedBackgroundMigrationStatus
  def initialize(gitlab_client, project_path)
    @gitlab_client = gitlab_client
    @project_path = project_path
  end

  attr_reader :gitlab_client, :project_path

  def notify(batched_migrations, label, notifier_class)
    batched_migrations.each do |migration|
      merge_request_id = migration['merge_request_id']

      begin
        merge_request ||= gitlab_client.merge_request(project_path, merge_request_id)
        author_username = merge_request.author.username

        next if merge_request.labels.include?(label)

        message = case notifier_class.to_s
                  when "FailedBatchedBackgroundMigration"
                    notifier_class.message(migration, author_username)
                  else
                    notifier_class.message(migration)
                  end

        gitlab_client.create_merge_request_note(project_path, merge_request_id, message)

        gitlab_client.update_merge_request(project_path, merge_request_id, add_labels: [label])
      rescue Net::OpenTimeout => e
        puts e.message
      end
    end
  end
end
