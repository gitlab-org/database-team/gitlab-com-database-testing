# frozen_string_literal: true

require 'yaml'

MERGE_REQUEST_PATH = 'https://gitlab.com/gitlab-org/gitlab/-/merge_requests/'

class BatchedBackgroundMigrationDictionary
  def initialize(file_path)
    @file_path = file_path
  end

  def execute
    return unless dictionary

    dictionary.merge(extra_values)
  end

  private

  def extra_values
    {
      "merge_request_id" => merge_request_id
    }
  end

  def dictionary
    @dictionary ||= load_dictionary
  end

  def merge_request_id
    dictionary['introduced_by_url'].gsub(MERGE_REQUEST_PATH, '')
  end

  def load_dictionary
    YAML::load_file(file_path)
  rescue Errno::ENOENT
    nil
  end

  attr_reader :file_path
end
