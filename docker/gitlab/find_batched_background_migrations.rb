# frozen_string_literal: true

require 'active_record'
require_relative 'lib/batched_background_migration_dictionary'

DICTIONARY_DIRECTORY = 'db/docs/batched_background_migrations/'

migration_status = ARGV[0]
output_file_name = ARGV[1]

raise('Invalid arguments') if migration_status.nil? || output_file_name.nil?

db_config = YAML::load_file('config/database.yml')['test']['main']
connection = ActiveRecord::Base.establish_connection(db_config).connection

batched_migrations = connection.select_values(<<~SQL)
  SELECT job_class_name from batched_background_migrations WHERE status = #{migration_status};
SQL

dictionaries = batched_migrations.map do |migration|
  file_path = File.join(DICTIONARY_DIRECTORY, "#{migration.underscore}.yml")

  BatchedBackgroundMigrationDictionary.new(file_path).execute
end

File.write("tmp/batched-background-migration-artifacts/#{output_file_name}.json", dictionaries.compact.map(&:to_h).to_json)
