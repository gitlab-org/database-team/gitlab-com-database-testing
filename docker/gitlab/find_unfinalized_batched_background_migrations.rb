# frozen_string_literal: true

require 'json'
require_relative 'lib/batched_background_migration_dictionary'

DICTIONARY_FOLDER_GLOB = Dir['db/docs/batched_background_migrations/*.yml']

dictionaries = DICTIONARY_FOLDER_GLOB.map { |file_path| BatchedBackgroundMigrationDictionary.new(file_path).execute }

File.write('tmp/batched-background-migration-artifacts/unfinalized_migrations_output.json', dictionaries.map(&:to_h).to_json)
