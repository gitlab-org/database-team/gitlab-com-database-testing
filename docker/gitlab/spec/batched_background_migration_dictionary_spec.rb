# frozen_string_literal: true

require 'spec_helper'

RSpec.describe BatchedBackgroundMigrationDictionary do
  subject(:dictionary) { described_class.new(file_path) }

  let(:file_path) { 'spec/fixtures/dictionary.yml' }
  let(:result) do
    {
      "migration_job_name" => 'BackfillPackagesTagsProjectId',
      "description" => 'Populates the new `packages_tags.project_id` column after joining with the `packages_packages` table',
      "feature_category" => 'package_registry',
      "introduced_by_url" => 'https://gitlab.com/gitlab-org/gitlab/-/merge_requests/135451',
      "merge_request_id" => '135451',
      "milestone" => 16.6,
      "queued_migration_version" => 20231030071209,
      "finalize_after" => '2023-12-23',
      "finalized_by" => nil
    }
  end

  describe '#execute' do
    it 'returns the batched background migration dictionary' do
      expect(dictionary.execute).to eql(result)
    end

    context 'when the dictionary file does not exist' do
      let(:file_path) { 'non_existing_dictionary' }

      it 'returns nil' do
        expect(dictionary.execute).to be_nil
      end
    end
  end
end
