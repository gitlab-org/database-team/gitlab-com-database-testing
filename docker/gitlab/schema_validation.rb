# frozen_string_literal: true

require 'gitlab/schema/validation'
require 'active_record'
require 'yaml'

class SchemaValidation
  def initialize(connection, file_path)
    @connection = connection
    @file_path = file_path
  end

  def run
    Gitlab::Schema::Validation::Runner.new(structure_sql, database, validators: validators).execute
  end

  private

  attr_reader :connection, :file_path

  def validators
    Gitlab::Schema::Validation::Validators::Base.all_validators
  end

  def structure_sql
    Gitlab::Schema::Validation::Sources::StructureSql.new(file_path)
  end

  def database
    Gitlab::Schema::Validation::Sources::Database.new(connection)
  end
end

db_config = YAML::load_file('config/database.yml')['test']['main']
connection = ActiveRecord::Base.establish_connection(db_config).connection

inconsistencies = SchemaValidation.new(connection, 'db/structure.sql').run

File.write('tmp/schema-validation-artifacts/output.json', inconsistencies.map(&:to_h).to_json)
