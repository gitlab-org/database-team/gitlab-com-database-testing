#!/bin/bash

set -e
BASE_PATH=$(dirname "$0")
for PATCH in "$BASE_PATH"/*.patch; do
  echo "Applying patch file ${PATCH}"
  # Testing patches often drift from master. Try to apply with a more
  # aggressive 3way merge strategy
  git am --3way < $PATCH
done
