# frozen_string_literal: true

$LOAD_PATH << (Pathname.new(Dir.pwd) + 'lib').to_s
$LOAD_PATH << (Pathname.new(Dir.pwd) + 'gems/gitlab-utils/lib').to_s

OUTPUT_DIR = 'tmp/table-sizes-artifacts'
OUTPUT_FILENAME = 'table-sizes-output.json'

class Rails
  def self.root
    Pathname.new(Dir.pwd)
  end

  def self.env
    obj = Object.new

    def obj.test?
      false
    end
    obj
  end
end

require 'active_record'
require 'active_support/concern'
require 'active_support/time'
require 'active_support/rescuable'

require_relative './lib/gitlab_edition'
require_relative './config/initializers/0_inject_enterprise_edition_module'
require_relative './app/models/concerns/ignorable_columns'
require 'gitlab/database/gitlab_schema'
require 'gitlab/database/dictionary'
require 'gitlab/database/shared_model'
require 'gitlab/database/postgres_table_size'
require 'gitlab/utils/strong_memoize'
require 'gitlab/database/database_connection_info'

require_relative './app/models/concerns/disables_sti'
require_relative './app/models/concerns/database_reflection'
require_relative './app/models/concerns/transactions'
require_relative './app/models/concerns/legacy_bulk_insert'
require_relative './app/models/concerns/cross_database_modification'
require_relative './app/models/concerns/gitlab/sensitive_attributes'
require_relative './app/models/concerns/gitlab/sensitive_serializable_hash'
require_relative './app/models/concerns/reset_on_column_errors'
require_relative './app/models/concerns/has_check_constraints'

begin # It raises an exception the first time but not the second
  require_relative './app/models/application_record'
rescue
  require_relative './app/models/application_record'
end

class TableSize
  DYNAMIC_SCHEMA = 'gitlab_partitions_dynamic'

  def initialize(connection)
    @connection = connection
  end

  def run
    Gitlab::Database::PostgresTableSize
      .alerting
      .where.not(schema_name: DYNAMIC_SCHEMA)
  end
end

db_config_main = YAML.load_file('config/database.yml', aliases: true)['test']['main']
main_connection = ActiveRecord::Base.establish_connection(db_config_main).connection

require 'gitlab/database'
require_relative './app/models/ci/application_record'
require 'gitlab/database/sec_application_record'

main_table_sizes = TableSize.new(main_connection).run.to_a
db_config_ci = YAML.load_file('config/database.yml', aliases: true)['test']['ci']
ci_connection = ActiveRecord::Base.establish_connection(db_config_ci).connection
ci_table_sizes = TableSize.new(ci_connection).run.to_a
table_sizes = main_table_sizes + ci_table_sizes

tmpdir = Pathname.new(Dir.pwd) + OUTPUT_DIR
tmpdir.mkdir unless tmpdir.exist?

File.open(tmpdir + OUTPUT_FILENAME, 'wb') do |file|
  file.write table_sizes.map(&:alert_report_hash).to_json
end
