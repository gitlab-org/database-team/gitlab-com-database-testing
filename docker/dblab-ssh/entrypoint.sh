#!/bin/bash

set -e

if [ -z "${DBLAB_SSH_KEY}" ] || [ "${DBLAB_SSH_KEY}" = "unset" ]; then
  echo "DBLAB_SSH_KEY not set"
  exit 1
fi

function dblab_ssh {
  ssh_opts=( -i ~/.ssh/id_rsa )

  if [ -n "${DBLAB_SSH_BASTION}" ]; then
    ssh_opts+=( -o "ProxyCommand=ssh ${DBLAB_SSH_BASTION} -i ~/.ssh/id_rsa -o StrictHostKeyChecking=off -W %h:%p" -o "StrictHostKeyChecking=off" )
  fi

  # shellcheck disable=SC2029
  ssh "${ssh_opts[@]}" "$@"
}

mkdir ~/.ssh
echo "${DBLAB_SSH_KEY}" > ~/.ssh/id_rsa
chmod 0400 ~/.ssh/id_rsa
echo "${DBLAB_HOST_KEYS}" > ~/.ssh/known_hosts

dblab_ssh -f -N -L 12345:127.0.0.1:"${DBLAB_API_PORT}" "${DBLAB_SSH_HOST}"

echo "Attempting to create clone ${DBLAB_CLONE_ID} on environment ${DBLAB_ENVIRONMENT}"

dblab init --url http://127.0.0.1:12345 --token "${DBLAB_TOKEN}" --environment-id "${DBLAB_ENVIRONMENT}"

dblab clone create --async --id "${DBLAB_CLONE_ID}" --username "${DBLAB_USER}" --password "${DBLAB_PASSWORD}"

# Hack to make sure that we wait long enough for the clone to at least be "CREATING"

sleep 5

until [[ "$(dblab clone status "${DBLAB_CLONE_ID}" | jq -r .status.code)" = "OK" ]]
do
  echo "Waiting for clone to be ready"
  sleep 5
done

dblab_info=$(dblab clone status "${DBLAB_CLONE_ID}")

port=$(echo "$dblab_info" | jq -r .db.port)
id=$(echo "$dblab_info" | jq -r .id)
createdAt=$(echo "$dblab_info" | jq -r .createdAt)
cloneStateTimestamp=$(echo "$dblab_info" | jq -r .snapshot.dataStateAt)
maxIdleMinutes=$(echo "$dblab_info" | jq -r .metadata.maxIdleMinutes)

echo "Opening port forwarding on port ${port}"

mkdir -p ~/webInfo
cat <<INFO > ~/webInfo/info.json
{
  "createdAt": "$createdAt",
  "cloneStateTimestamp": "$cloneStateTimestamp",
  "cloneId": "$id",
  "maxIdleMinutes": $maxIdleMinutes
}
INFO
webfsd -p 8000 -r ~/webInfo

echo "Started server with clone info on port 8000"

# This blocks and opens port forwarding for Postgres
dblab_ssh -N -L ":5432:127.0.0.1:$port" "${DBLAB_SSH_HOST}"
