#!/bin/bash

set -e

# We need to `git init` again because we run inside the GDK image and this
# `gitlab` directory is has had the `.git` directory removed to keep it
# smaller. It also means we need the `reset --hard` and `clean -fd` to get us
# correctly to the current state of the default branch.
git init --initial-branch=master
git remote add housekeeper "https://gitlab-ci-token:${GITLAB_API_TOKEN}@${HOUSEKEEPER_REPO_URL}"
# We only need to fetch `master` and no other branches. We cannot use
# `--depth=1` due to pushes 524 timeout later on.
git fetch housekeeper master --filter=tree:0
git reset --hard housekeeper/master
git clean -fd

export HOUSEKEEPER_GITLAB_API_TOKEN=$GITLAB_API_TOKEN
export POSTGRES_AI_PASSWORD=$DBLAB_PASSWORD

# We run housekeeper for each database to ensure we are checking the batched
# background migration state against the correct tracking database
for db_host in $(echo "$ALL_DB_HOSTS" | tr ',' '\n'); do
    export POSTGRES_AI_CONNECTION_STRING="host=$db_host user=${DBLAB_USER} dbname=gitlabhq_dblab"

    bundle exec gitlab-housekeeper -m10 \
      -k Keeps::OverdueFinalizeBackgroundMigration

    if [ "$(date +%w)" = "0" ]; then
      bundle exec gitlab-housekeeper -m1 -k Keeps::UpdateTableSizes
    fi
done
