#!/bin/bash

set -e

# Converts project namespace to its variable name:
# gitlab-org/security/gitlab -> GITLAB_ORG_SECURITY_GITLAB_API_TOKEN
TOKEN_NAME=$(echo "$TOP_UPSTREAM_SOURCE_PROJECT" | sed -e 's/[^a-zA-Z0-9]/_/g' | tr '[:lower:]' '[:upper:]' | sed "s/$/_API_TOKEN/g")

# If the TOKEN_NAME exists, uses the token to fetch from TOP_UPSTREAM_SOURCE_PROJECT
if test "${!TOKEN_NAME}"; then
  echo "https://gitlab-ci-token:${!TOKEN_NAME}@gitlab.com/${TOP_UPSTREAM_SOURCE_PROJECT}.git"
else
  echo "http://gitlab.com/gitlab-org/gitlab.git"
fi
